'''Please note that non standard characters will probably not render properly'''

The complete helpfile is over 50kB in size.  Some browsers may have difficulty in rendering the page, because many have a notional upper limit of 32kB per page.  Please choose from one of the following options to access the help file:

== Related Math Help pages ==

* [[Complete math help file]] - over 50kB in size

* [[Math Help part 1]] - contains information on Syntax, Rendering and Pros and Cons of TeX and HTML for math functions

* [[Math Help part 2]] - contains information on Functions, Symbols and Special characters

* [[Math Help part 3]] - contains information on Subscripts, Superscripts and Integrals

* [[Math Help part 5]] - contains information on Alphabets and Typefaces

* [[Math Help part 6]] - contains information on Parenthesising big exprssions, Brackets and Bars

* [[Math Help part 7]] - contains information about Spacing, Aligning with normal text flow,Forced PNG rendering, Color and examples.

This wiki uses a subset of TeX markup, including some extensions from LaTeX and AMS-LaTeX, for mathematical formulae. It generates either PNG images or simple HTML markup, depending on user preferences and the complexity of the expression. In the future, as more browsers are smarter, it will be able to generate enhanced HTML or even MathML in many cases.  

More precisely, MediaWiki filters the markup through Texvc, which in turn passes the commands to TeX for the actual rendering. Thus, only a limited part of the full TeX language is supported; see below for details.

__TOC__



== Fractions, matrices, multilines == 
<table class="wikitable">

<tr>
<th>Feature</th>
<th>Syntax</th>
<th>How it looks rendered</th>
</tr>

<tr>
<td>Fractions</td>
<td><code>\frac{2}{4}=0.5</code></td>
<td><math>\frac{2}{4}=0.5</math></td>
</tr>


<tr>
<td>Large (nestled) Fractions</td>
<td><code>\cfrac{2}{c + \cfrac{2}{d + \cfrac{2}{4}}} = a</code></td>
<td><math>\cfrac{2}{c + \cfrac{2}{d + \cfrac{2}{4}}} = a</math></td>
</tr>


<tr>
<td rowspan="7">Matrices</td>
<td><pre>\begin{matrix}
  x & y \\
  z & v 
\end{matrix}</pre></td>
<td><math>\begin{matrix} x & y \\ z & v
\end{matrix}</math></td>
</tr>

<tr>
<td><pre>\begin{vmatrix}
  x & y \\
  z & v 
\end{vmatrix}</pre></td>
<td><math>\begin{vmatrix} x & y \\ z & v
\end{vmatrix}</math></td>
</tr>

<tr>
<td><pre>\begin{Vmatrix}
  x & y \\
  z & v
\end{Vmatrix}</pre></td>
<td><math>\begin{Vmatrix} x & y \\ z & v
\end{Vmatrix}</math></td>
</tr>

<tr>
<td><pre>\begin{bmatrix}
  0      & \cdots & 0      \\
  \vdots & \ddots & \vdots \\ 
  0      & \cdots & 0
\end{bmatrix}</pre></td>
<td><math>\begin{bmatrix} 0 & \cdots & 0 \\ \vdots
& \ddots & \vdots \\ 0 & \cdots &
0\end{bmatrix} </math></td>
</tr>

<tr>
<td><pre>\begin{Bmatrix}
  x & y \\
  z & v
\end{Bmatrix}</pre></td>
<td><math>\begin{Bmatrix} x & y \\ z & v
\end{Bmatrix}</math></td>
</tr>

<tr>
<td><pre>\begin{pmatrix}
  x & y \\
  z & v 
\end{pmatrix}</pre></td>
<td><math>\begin{pmatrix} x & y \\ z & v
\end{pmatrix}</math></td>
</tr>

<tr>
<td>Case distinctions</td>
<td><pre>
f(n) = 
\begin{cases} 
  n/2,  & \mbox{if }n\mbox{ is even} \\
  3n+1, & \mbox{if }n\mbox{ is odd} 
\end{cases}</pre></td>
<td><math>f(n) = 
\begin{cases}
  n/2,  & \mbox{if }n\mbox{ is even} \\ 
  3n+1, & \mbox{if }n\mbox{ is odd} 
\end{cases} </math></td>
</tr>

<tr>
<td>Breaking up a long expression so that it wraps when necessary</td>
<td><pre>
<nowiki>
<math>f(x) \,\!</math>
<math>= \sum_{n=0}^\infty a_n x^n </math>
<math>= a_0+a_1x+a_2x^2+\cdots</math>
</nowiki>
</pre>
</td>
<td>
<math>f(x) \,\!</math><math>= \sum_{n=0}^\infty a_n x^n </math><math>= a_0 +a_1x+a_2x^2+\cdots</math>
</td>
</tr>

<tr>
<td>Simultaneous equations</td>
<td><pre>\begin{cases}
    3x + 5y +  z \\
    7x - 2y + 4z \\
   -6x + 3y + 2z 
\end{cases}</pre></td>
<td><math>\begin{cases} 3x + 5y + z \\ 7x - 2y + 4z \\ -6x + 3y + 2z \end{cases}</math></td>
</tr>

</table>
