Other VHF/UHF/Microwave wiki pages : [[13 centimetres]], [[9 centimetres]], [[6 centimetres]], [[3 centimetres]] [[1.25 centimetres]] and [[Bands above 24GHz]].

{{Band
|band=2m
|USG=144.000-148.000
|USE=144.000-148.000
|USA=144.000-148.000
|UST=144.000-148.000
|UK=144-146
}}

2 metres is probably the most heavily-used amateur band in the United States.  Most newly-licensed [[Technician|Technician-class]] operators get their start in amateur radio on 2 meter [[Modes#Frequency Modulation (FM)|FM]].

==Equipment==
Equipment for operation on two meters is plentiful; many well-known manufacturers produce new base, mobile and hand-held transceivers intended specifically for the 2 meter amateur radio band. In some cases, additional bands (most often [[70 centimeters]] but occasionally also additional VHF bands) will be supported.

Much of the early equipment deployed by radio amateurs on 2 meter FM was surplus, built originally for commercial land-mobile use on bands elsewhere in the 130-174MHz spectrum and converted to the 144-148MHz amateur band. Users of adjacent spectrum range from marine radiotelephone and public safety to dispatch radios used by taxis and service vehicles. As commercial two-way radio frequencies became more crowded, spacing between channels was reduced, leading to many existing radios becoming available as surplus. The oldest of this equipment was crystal-controlled and provided use of a limited number of FM voice frequencies; modern equipment is frequency-synthesized and often also capable of receiving out-of-band services such as weather and marine radio. 

Commercial land-mobile base station equipment has also been adapted readily to construct repeaters, although factory-built repeaters designed specifically for amateur use are now commercially available. This ready availability of a wide selection of equipment at reasonable cost has made 2 meters the most commonly-used amateur band for mobile voice communication.

==Modulation==
The most common application of 2 meter radio is in FM voice transmission, either operated directly between stations or via automated [[repeater]]s. Individual FM channels are normally spaced at 15kHz or 20kHz intervals. Repeater inputs below 147MHz are typically set 600kHz below the output frequency; above 147MHz this pattern is reversed.

The 2 meter band is a popular choice for digital packet transmission, with 144.39MHz (North America) and 144.8MHz (Europe) commonly used for [[APRS]] operation.

==Propagation==
Signals in this band travel primarily line-of-sight or slightly further. It is not uncommon for a powerful, well-situated repeater to be accessible to mobile stations at up to fifty miles distance. During unusual atmospheric conditions (such as temperature inversion) the VHF signals' range may be significantly but temporarily increased.

==Australian Bandplan==

[[Image:vk4yeh_vk_2m_bandplan.jpg]]


{{bands}}
