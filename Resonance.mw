{{Template:Stub}}

Related wiki pages: [[Antennas]], [[capacitors|capacitance]], [[inductors|inductance]]

An antenna is a type of tuned circuit consisting of both [[capacitors|capacitance]] and [[inductors|inductance]].  At resonance, capacitive and inductive impedance are equally balanced - in a sense canceling each other out. At this point the antenna appears to be entirely resistive. The apparent resistance is a combination of loss resistance (for example in the feedlines and antenna elements) and radiation resistance.

[[Image:Vk4yeh_antenna_resonance.jpg|600px]]

The inductance and capacitance of an antenna are determined by a number of factors, including construction materials, height above earth, and its dimensions. Of these, dimensions are usually the major feature affecting resonant frequency. For example, operating on [[HF]] requires an antenna with much larger elements that when operating on [[VHF]].

Ideally, antennas are operated close to their resonant frequency. However, this would effectively result in a limited bandwidth being available for use.

Possible solutions to this problem are to use a "thicker" element, or to use a different antenna type.


{{electronics}}
