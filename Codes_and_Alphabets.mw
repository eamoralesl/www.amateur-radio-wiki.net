== Phonetic Alphabet ==

A number of phonetic alphabets exist. The NATO version is most common and can be considered to be the "international" phonetic alphabet.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-
! Letter !!align=left| Code word !!align=left| Pronunciation
|-
|align=center| A || '''Alpha''' ||'''AL''' FAH
|-
|align=center| B ||'''Bravo''' ||'''BRAH''' VOH 
|-
|align=center| C ||'''Charlie''' ||'''CHAR''' LEE 
|- 
|align=center| D ||'''Delta''' ||'''DELL''' TAH
|- 
|align=center| E ||'''Echo''' ||'''ECK''' OH
|-
|align=center| F ||'''Foxtrot''' ||'''FOKS''' TROT 
|-
|align=center| G ||'''Golf''' ||'''GAHLF'''
|-
|align=center| H ||'''Hotel''' ||HO '''TELL''' 
|-
|align=center| I ||<!--Do not change; it's India, not indigo-->'''India''' ||'''IN''' DEE AH
|-
|align=center| J ||'''Juliet''' ||'''JEW''' LEE '''ETT'''
|-
|align=center| K ||'''Kilo''' ||'''KEY''' LOH
|-
|align=center| L ||'''Lima''' ||'''LEE''' MAH
|-
|align=center| M ||'''Mike''' ||MIKE
|-
|align=center| N ||'''November''' ||NO '''VEM''' BER
|-
|align=center| O ||'''Oscar''' ||'''OSS''' CAH 
|-
|align=center| P ||'''Papa''' ||PAH '''PAH'''
|-
|align=center| Q ||'''Quebec''' ||KEH '''BECK'''
|-
|align=center| R ||'''Romeo''' ||'''ROW''' ME OH
|-
|align=center| S || '''Sierra''' ||SEE '''AIR''' RAH 
|-
|align=center| T ||'''Tango''' ||'''TANG''' GO
|-
|align=center| U ||'''Uniform''' ||'''YOU''' NEE FORM 
|-
|align=center| V ||'''Victor''' ||'''VIK''' TAH 
|-
|align=center| W ||'''Whiskey''' ||'''WISS''' KEY
|-
|align=center| X ||'''X-ray''' or<br> '''Xray''' ||'''ECKS RAY''' 
|-
|align=center| Y ||'''Yankee''' ||'''YANG''' KEY
|-
|align=center| Z ||'''Zulu''' ||'''ZOO''' LOO
|-
|align=center|  ||   ||
|-
|align=center| '''Number''' ||  '''Code word''' || '''Pronunciation'''
|-
|align=center| 0  ||Zero ||ZE RO 
|-
|align=center| 1 || One ||WUN 
|-
|align=center| 2 || Two ||TOO 
|-
|align=center| 3 || Three ||TREE 
|-
|align=center| 4 ||Four ||'''FOW''' ER 
|-
|align=center| 5 ||Five ||FIFE 
|-
|align=center| 6 ||Six ||SIX 
|-
|align=center| 7 ||Seven ||'''SEV''' EN 
|-
|align=center| 8 ||Eight ||AIT 
|-
|align=center| 9 ||Nine ||'''NIN''' ER 
|}

== Morse Code ==

''Main articles: [[morse code]],  [[Wikipedia:Morse code]]''

Morse code is a way to encode text through the generation of a carrier wave ([[CW]]). It is used to communicate over long distances or with low power ([[QRP]]).

You do not need to learn morse code to obtain a radio license or operate an amateur radio station anymore.

The code is composed of 5 elements:

# short mark, dot or 'dit' (·) — one unit long
# longer mark, dash or 'dah' (–) — three units long
# intra-character gap (between the dots and dashes within a character) — one unit long
# short gap (between letters) — three units long
# medium gap (between words) — seven units long

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
! Character || Code || Character || Code || Character || Code
! Character || Code || Character || Code || Character || Code
|-
| '''A''' || '''· —'''
| '''J''' || '''· — — —'''
| '''S''' || '''· · ·'''
| '''1''' || '''· — — — —'''
| '''.''' || '''· — · — · —'''
| ''':''' || '''— — — · · ·'''
|-
| '''B''' || '''— · · ·'''
| '''K''' || '''— · —'''
| '''T''' || '''—'''
| '''2''' || '''· · — — —'''
| ''',''' || '''— — · · — —'''
| ''';''' || '''— · — · — ·'''
|-
| '''C''' || '''— · — ·'''
| '''L''' || '''· — · ·'''
| '''U''' || '''· · —'''
| '''3''' || '''· · · — —'''
| '''?''' || '''· · — — · ·'''
| '''=''' || '''— · · · —'''
|-
| '''D''' || '''— · ·'''
| '''M''' || '''— —'''
| '''V''' || '''· · · —'''
| '''4''' || '''· · · · —'''
| <!--Apostrophe (punctuation)--> ''''''' || '''· — — — — ·'''
| '''+''' || '''· — · — ·'''
|-
| '''E''' || '''·'''
| '''N''' || '''— ·'''
| '''W''' || '''· — —'''
| '''5''' || '''· · · · ·'''
| '''!''' || '''— · — · — —'''
| <!--Hyphen--> '''-''' || '''— · · · · —'''
|-
| '''F''' || '''· · — ·'''
| '''O''' || '''— — —'''
| '''X''' || '''— · · —'''
| '''6''' || '''— · · · ·'''
| '''/''' || '''— · · — ·'''
| <!--Underscore--> '''_''' || '''· · — — · —'''
|-
| '''G''' || '''— — ·'''
| '''P''' || '''· — — ·'''
| '''Y''' || '''— · — —'''
| '''7''' || '''— — · · ·'''
| '''(''' || '''— · — — ·'''
| <!--Quotation mark--> '''"''' || '''· — · · — ·'''
|-
| '''H''' || '''· · · ·'''
| '''Q''' || '''— — · —'''
| '''Z''' || '''— — · ·'''
| '''8''' || '''— — — · ·'''
| ''')''' || '''— · — — · —'''
| '''$''' || '''· · · — · · —'''
|-
| '''I''' || '''· ·'''
| '''R''' || '''· — ·'''
| '''0''' || '''— — — — —'''
| '''9''' || '''— — — — ·'''
| '''&''' || '''· — · · ·'''
| '''@''' || '''· — — · — ·'''
|}

== Q-Code ==

These codes were originally developed to shorten transmission times when using CW, but are frequently used in voice transmissions. (eg. ''I am going to go QRT, thanks for the QSO.'')

The ''QRA''...''QUZ'' code range includes phrases applicable to all services and is allocated to the International Telecommunications Union. NATO's [http://www.armymars.net/ArmyMARS/DigitalOps/Resources/acp131-operating-sigs.pdf ACP 131(E)], COMMUNICATIONS INSTRUCTIONS - OPERATING SIGNALS, March 1997, chapter 2 contains a full list of 'Q' codes. Other 'Q' code ranges are allocated specifically to aviation or maritime services; many of those codes have fallen into disuse as voice displaces CW in commercial operation.

The Q-code was originally instituted at the Radiotelegraph Convention held in London, 1912 and was intended for marine radiotelegraph use. The codes were based on an earlier list published by the British postmaster general's office in 1908.<sup>[http://www.kloth.net/radio/qcodes.php]</sup> More information about the history and usage of Q-codes can be found [http://en.wikipedia.org/wiki/Q_code here].

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" summary="Most common Q codes used in the amateur radio service, including meaning and sample use"
|----- align="left"
! width="50" align="left"| Code
! Meaning
! Sample use
|+ '''Q Codes Commonly Used by Radio Amateurs'''
|-----
| valign="top" | QRG || valign="top" | Exact frequency
| valign="top" | HE TX ON QRG 14205 kHz
|-----
| valign="top" | QRI
| valign="top" | Tone (T in the RST code)
| valign="top" | UR QRI IS 9
|-----
| valign="top" | QRK
| valign="top" | Intelligibility (R in the RST code)
| valign="top" | UR QRK IS 5
|-----
| valign="top" | QRL
| valign="top" | This frequency is busy.
| valign="top" | Used almost exclusively with morse code, usually as a question (QRL? - is this frequency busy?) before transmitting on a new frequency
|-----
| valign="top" | [[QRM]]
| valign="top" | Man-made interference
| valign="top" | ANOTHER QSO UP 2 kHz CAUSING LOT OF QRM
|-----
| valign="top" | [[QRN]]
| valign="top" | Natural interference, e.g. static crashes
| valign="top" | BAND NOISY TODAY LOT OF QRN
|-----
| valign="top" | QRO || valign="top" | Increase power
| valign="top" | NEED QRO WHEN PROP POOR
|-----
| valign="top" | [[QRP]]
| valign="top" | Decrease power || valign="top" | QRP TO 5 W (As a mode of operation, a QRP station is five watts or less, a QRPp station one watt or less)
|-----
| valign="top" | QRQ
| valign="top" | Send more quickly
| valign="top" | TIME SHORT PSE QRQ
|-----
| valign="top" | QRR
| valign="top" | Temporarily unavailable/away, please wait
| valign="top" | WILL BE QRR 30 MIN = THAT STN IS QRR NW
|-----
| valign="top" | QRRR || valign="top" | Land distress
| valign="top" | A non-standard call proposed by ARRL for land-based or railroad emergency traffic in situations where response from ships at sea (which listened for <span style="text-decoration: overline">SOS</span>) was neither needed nor desired.<sup>[http://www.qsl.net/ae0q/sos.htm]</sup><sup>[http://www.arrl.org/tis/info/history.html#sos]</sup> Now deprecated.
|-----
| valign="top" | [[QRSS|QRS]] || valign="top" | Send more slowly
| valign="top" | PSE QRS NEW TO CW (QRS operation - a slower dot rate - is useful during weak-signal conditions; a QRSS mode uses an extremely low code rate on a channel less than 1Hz wide to allow reception under extreme QRP conditions)
|-----
| valign="top" | QRT || valign="top" | Stop sending
| valign="top" | ENJOYED TALKING 2 U = MUST QRT FER DINNER NW
|-----
| valign="top" | QRU || valign="top" | Have you anything for me?
| valign="top" | QRU? ABOUT TO QRT
|-----
| valign="top" | QRV || valign="top" | I am ready
| valign="top" | WL U BE QRV IN UPCOMING CONTEST?
|-----
| valign="top" | QRX || valign="top" | Will call you again
| valign="top" | QRX @ 1500H
|-----
| valign="top" | QRZ || valign="top" | You are being called by ________.
| valign="top" | QRZ? UR VY WEAK (Only someone who has previously called should reply)
|-----
| valign="top" | QSA || valign="top" | Signal strength
| valign="top" | UR QSA IS 5
|-----
| valign="top" | QSB || valign="top" | Fading of signal
| valign="top" | THERE IS QSB ON UR SIG
|-----
| valign="top" | QSD
| valign="top" | Your keying is defective
| valign="top" | QSD CK YR TX
|-----
| valign="top" | QSK || valign="top" | Break-in
| valign="top" | I CAN HR U DURING MY SIGS PSE QSK
|-----
| valign="top" | [[QSL]]
| valign="top" | I Acknowledge receipt
| valign="top" | QSL UR LAST TX = PSE QSL VIA BURO (i.e. please send me a card confirming this contact).
|-----
| valign="top" | QSM || valign="top" | Repeat last message
| valign="top" | QRM DROWNED UR LAST MSG OUT = PSE QSM
|-----
| valign="top" | QSN || valign="top" | I heard you
| valign="top" | QSN YESTERDAY ON 7005 kHz
|-----
| valign="top" | QSO || valign="top" | A conversation
| valign="top" | TNX QSO 73
|-----
| valign="top" | QSP || valign="top" | Relay
| valign="top" | PSE QSP THIS MSG TO MY FRIEND
|-----
| valign="top" | QST
| valign="top" | General call to all stations
| valign="top" | QST: QRG ALLOCS HV CHGD
|-----
| valign="top" | QSX
| valign="top" | I am listening on ... frequency
| valign="top" | QSX 14200 TO 14210 kHz
|-----
| valign="top" | [[QSY]]
| valign="top" | Shift to transmit on ...
| valign="top" | LETS QSY UP 5 kHz
|-----
| valign="top" | QTA
| valign="top" | Disregard last message
| valign="top" | QTA, DID NOT MEAN THAT
|-----
| valign="top" | QTC
| valign="top" | Traffic
| valign="top" | STN WID EMRG QTC PSE GA
|-----
| valign="top" | [[QTH]] || valign="top" | Location
| valign="top" | QTH IS SOUTH PARK CO
|-----
| valign="top" | QTR || valign="top" | Exact time
| valign="top" | QTR IS 2000 Z
|}


== RST code ==
The RST code, in its original form, is intended for CW operation. On SSB, the final digit (tone) is normally omitted.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" summary="RST Code" 
|----- align="left"
! width="50" align="left" | Number
! R - Readability
! S - Strength
! T - Tone
|+ '''RST Code Commonly Used by Radio Amateurs'''
|-----
| valign="top" | 1 
| valign="top" | Unreadable
| valign="top" | Faint signal, barely perceptible
| valign="top" | Sixty cycle a.c or less, very rough and broad
|-----
| valign="top" | 2
| valign="top" | Barely readable, occasional words distinguishable
| valign="top" | Very Weak
| valign="top" | Very rough a.c., very harsh and broad
|-----
| valign="top" | 3
| valign="top" | Readable with considerable difficulty
| valign="top" | Weak
| valign="top" | Rough a.c. tone, rectified but not filtered
|-----
| valign="top" | 4
| valign="top" | Readable with practically no difficulty
| valign="top" | Fair
| valign="top" | Rough note, some trace of filteringfrequency
|-----
| valign="top" | 5
| valign="top" | Perfectly readable
| valign="top" | Fairly Good
| valign="top" | Filtered rectified a.c. but strongly ripple-modulated
|-----
| valign="top" | 6
| valign="top" | not used
| valign="top" | Good
| valign="top" | Filtered tone, definite trace of ripple modulation
|-----
| valign="top" | 7 
| valign="top" | not used
| valign="top" | Moderately Strong
| valign="top" | Near pure tone, trace of ripple modulation
|-----
| valign="top" | 8
| valign="top" | not used
| valign="top" | Strong
| valign="top" | Near perfect tone, slight trace of modulation
|-----
| valign="top" | 9
| valign="top" | not used
| valign="top" | Very strong signals
| valign="top" | Perfect tone, no trace of ripple or modulation of any kind
|
|}

In CW operation, individual digits may be abbreviated by substituting as follows: 1 = A,   2 = U,   3 = V,   4 = 4,   5 = E,   6 = 6,   7 = B,   8 = D,   9 = N,   0 = T (for instance, RST 599 could be sent as 5NN - a shorter message in CW). These are referred to as "cut numbers" and are obtained by replacing all of the dashes in a CW digit with a single dash. Cut numbers are not suitable for transmitting data which already contains mixed alphanumerics, such as [[callsign]]s.<sup>[http://www.qsl.net/zs1an/contesting_faq.html#cut-numbers]</sup>

== RSQ code ==  

Often used to describe reception and quality of digital modes such as PSK31

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" summary="RSV Code" 
|----- align="left"
! width="50" align="left" | Number
! R - Readability
! S - Strength
! V - Quality
|+ '''RSQ Code Commonly Used by Radio Amateurs'''
|-----
| valign="top" | 1 
| valign="top" | 0% copy - undecipherable
| valign="top" | barely perceptible trace
| valign="top" | splatter over much of the spectrum
|-----
| valign="top" | 2
| valign="top" | 20% copy -occasional words distinguishable
| valign="top" | not used
| valign="top" | not used
|-----
| valign="top" | 3
| valign="top" | 40% copy - readable with difficulty, many missed characters
| valign="top" | Weak trace
| valign="top" | multiple visible pairs
|-----
| valign="top" | 4
| valign="top" | 80% copy - Readable with no difficulty
| valign="top" | not used
| valign="top" | not used
|-----
| valign="top" | 5
| valign="top" | 95%+ copy - Perfectly readable
| valign="top" | Moderate trace
| valign="top" | One easily visible pair
|-----
| valign="top" | 6
| valign="top" | not used
| valign="top" | not used
| valign="top" | not used
|-----
| valign="top" | 7 
| valign="top" | not used
| valign="top" | Strong Trace
| valign="top" | One barely visible pair
|-----
| valign="top" | 8
| valign="top" | not used
| valign="top" | not used
| valign="top" | not used
|-----
| valign="top" | 9
| valign="top" | not used
| valign="top" | Very strong trace
| valign="top" | Clean signal - no visible unwanted sidebars
|
|}

== RSV code for SSTV transmissions ==


{| class="wikitable" style="line-height: 115%;" summary="RSV Code" 
|----- align="left"
! width="50" align="left" | Number
! R - Readability
! S - Strength
! V - Video
|+ '''RSV Code Commonly Used by Radio Amateurs'''
|-----
| valign="top" | 1 
| valign="top" | Unreadable
| valign="top" | Faint signal, barely perceptible
| valign="top" | Picture unreadable
|-----
| valign="top" | 2
| valign="top" | Barely readable
| valign="top" | Very Weak
| valign="top" | picture barely visible
|-----
| valign="top" | 3
| valign="top" | Readable with difficulty
| valign="top" | Weak
| valign="top" | Readable with flaws
|-----
| valign="top" | 4
| valign="top" | Readable with no difficulty
| valign="top" | Fair
| valign="top" | Very good picture some flaws
|-----
| valign="top" | 5
| valign="top" | Perfectly readable
| valign="top" | Fairly Good
| valign="top" | Perfect picture no flaws
|-----
| valign="top" | 6
| valign="top" | not used
| valign="top" | Good
| valign="top" | not used
|-----
| valign="top" | 7 
| valign="top" | not used
| valign="top" | Moderately Strong
| valign="top" | not used
|-----
| valign="top" | 8
| valign="top" | not used
| valign="top" | Strong
| valign="top" | not used
|-----
| valign="top" | 9
| valign="top" | not used
| valign="top" | Very strong signals
| valign="top" | not used
|
|}

In fast-scan amateur television (ATV), signal-to-noise ratio is reported as one of:
:P0 - all image detail lost
:P1 - 3-8dB, barely legible
:P2 - 8-20dB, definitely noisy
:P3 - 20-35dB, somewhat noisy
:P4 - 35-45dB, slightly noisy
:P5 - 45dB+, no discernible noise<sup>[http://www.ussc.com/~uarc/utah_atv/psandqs.html]</sup>

== CW Abbreviations ==

These abbreviations are commonly used in CW transmissions to shorten transmission times. Not all CW operators use all of them - most will use very few. As a general rule most operators do not abbreviate unnecessarily, especially when communication with an operator that they do not know or whose experience is unknown. In contest conditions, abbreviations are common as operators try to gain as many contacts as possible over the competition period.

{| class="wikitable" style="line-height: 115%;" summary="CW abbreviations"
|----- align="left"
! width="50" align="left"| Abbreviation
! Meaning
! Abbreviation
! Meaning
|+ '''CW Abbreviations'''
|-----
| valign="top" | AA 
| valign="top" | All After
| valign="top" | OB
| valign="top" | Old Boy
|-----
| valign="top" | AB
| valign="top" | All Before
| valign="top" | OC
| valign="top" | Old Chap
|-----
| valign="top" | ABT
| valign="top" | About
| valign="top" | OM 
| valign="top" | Old Man
|-----
| valign="top" | ADEE
| valign="top" | Addressee
| valign="top" | OP
| valign="top" | Operator
|-----
| valign="top" | ADR
| valign="top" | Address
| valign="top" | OPR
| valign="top" | Operator
|-----
| valign="top" | AGN
| valign="top" | Again
| valign="top" | OT
| valign="top" | Old Timer
|-----
| valign="top" | AM
| valign="top" | Amplitude Modulation
| valign="top" | PBL
| valign="top" | Preamble
|-----
| valign="top" | ANT
| valign="top" | Antenna 
| valign="top" | PKG
| valign="top" | Package
|-----
| valign="top" | BCI
| valign="top" | Broadcast Interference
| valign="top" | PSE
| valign="top" | Please
|-----
| valign="top" | BCL
| valign="top" | Broadcast listener
| valign="top" | PT
| valign="top" | Point
|-----
| valign="top" | BCNU
| valign="top" | Be seeing you
| valign="top" | PWR
| valign="top" | Power
|-----
| valign="top" | BK
| valign="top" | Break in
| valign="top" | PX
| valign="top" | Press
|-----
| valign="top" | BN
| valign="top" | Between, Been
| valign="top" | R
| valign="top" | Received, Are
|-----
| valign="top" | BT 
| valign="top" | Separation
| valign="top" | RC
| valign="top" | Ragchew
|-----
| valign="top" | BTR
| valign="top" | Better
| valign="top" | RCD
| valign="top" | Received
|-----
| valign="top" | Bug
| valign="top" | Semi automatic key
| valign="top" | RCVR
| valign="top" | Receiver
|-----
| valign="top" | C
| valign="top" | Yes, Correct
| valign="top" | REF
| valign="top" | Refer to
|-----
| valign="top" | CFM
| valign="top" | Confirm, I confirm
| valign="top" | RFI
| valign="top" | Radio Frequency Interference
|-----
| valign="top" | CK
| valign="top" | Check
| valign="top" | RIG
| valign="top" | Station Equipment
|-----
| valign="top" | CKT
| valign="top" | Circuit
| valign="top" | RPT
| valign="top" | Repeat, Report
|-----
| valign="top" | CL
| valign="top" | Closing Station,  Call
| valign="top" | RTTY
| valign="top" | Radioteletype
|-----
| valign="top" | CLBK
| valign="top" | Callbook
| valign="top" | RST
| valign="top" | Readability Strength Tone
|-----
| valign="top" | CLD
| valign="top" | Called
| valign="top" | RX
| valign="top" | Receive, receiver
|-----
| valign="top" | CLG
| valign="top" | Calling
| valign="top" | SASE
| valign="top" | Self addressed stamped envelope
|-----
| valign="top" | CNT
| valign="top" | Cant
| valign="top" | SED
| valign="top" | Said
|-----
| valign="top" | CONDX
| valign="top" | Conditions
| valign="top" | SEZ
| valign="top" | Says
|-----
| valign="top" | CQ
| valign="top" | Calling any station
| valign="top" | SGD
| valign="top" | Signed
|-----
| valign="top" | CU
| valign="top" | See you
| valign="top" | SIG
| valign="top" | Signature, Signal
|-----
| valign="top" | CUL
| valign="top" | See you later
| valign="top" | SINE
| valign="top" | Personal initials or nickname
|-----
| valign="top" | CUM
| valign="top" | Come
| valign="top" | SKED
| valign="top" | Schedule
|-----
| valign="top" | CW
| valign="top" | Continuous Wave
| valign="top" | SRI
| valign="top" | Sorry
|-----
| valign="top" | DA
| valign="top" | day
| valign="top" | SS
| valign="top" | Sweepstakes
|-----
| valign="top" | DE
| valign="top" | From, From this
| valign="top" | SSB
| valign="top" | Single Sideband
|-----
| valign="top" | DIFF
| valign="top" | Difference
| valign="top" | STN
| valign="top" | Station
|-----
| valign="top" | DLD & DLVD
| valign="top" | Delivered
| valign="top" | SUM
| valign="top" | Some
|-----
| valign="top" | DN
| valign="top" | Down
| valign="top" | SVC
| valign="top" | Service
|-----
| valign="top" | DR
| valign="top" | Delivered
| valign="top" | T
| valign="top" |Zero
|-----
| valign="top" | DX
| valign="top" | Distance
| valign="top" | TFC
| valign="top" |Traffic
|-----
| valign="top" | EL
| valign="top" | Element
| valign="top" | TMW
| valign="top" |Tomorrow
|-----
| valign="top" | ES
| valign="top" | And
| valign="top" | TKS & TNX
| valign="top" |Thanks
|-----
| valign="top" | FB
| valign="top" | Fine business
| valign="top" | TR & TX
| valign="top" |Transmit
|-----
| valign="top" | FER
| valign="top" | For
| valign="top" | T/R
| valign="top" |Transmit/Receive
|-----
| valign="top" | FM
| valign="top" | Frequency Modulation, From
| valign="top" | TRIX
| valign="top" |Tricks
|-----
| valign="top" | GA
| valign="top" | Go ahead, Good afternoon
| valign="top" | TT
| valign="top" |That
|-----
| valign="top" | GB
| valign="top" | Goodbye, God Bless
| valign="top" | TTS
| valign="top" |That is
|-----
| valign="top" | GD
| valign="top" | Good
| valign="top" | TU
| valign="top" |Thank you
|-----
| valign="top" | GE
| valign="top" | Good Evening
| valign="top" | TVI
| valign="top" |Television interference
|-----
| valign="top" | GESS
| valign="top" | Guess
| valign="top" | TX
| valign="top" | Transmitter, Transmit
|-----
| valign="top" | GG
| valign="top" | Going
| valign="top" | TXT
| valign="top" | text
|-----
| valign="top" | GM
| valign="top" | Good Morning
| valign="top" | U
| valign="top" | You
|-----
| valign="top" | GN
| valign="top" | Good Night
| valign="top" | UR
| valign="top" | You're Your
|-----
| valign="top" | GND
| valign="top" | Ground
| valign="top" | URS
| valign="top" | Yours
|-----
| valign="top" | GUD
| valign="top" | Good
| valign="top" | VFB
| valign="top" | Very Fine Business
|-----
| valign="top" | GV
| valign="top" | Give
| valign="top" | VFO
| valign="top" | Variable Frequency Oscillator
|-----
| valign="top" | HH
| valign="top" | Error sending
| valign="top" | VY
| valign="top" | Very
|-----
| valign="top" | HI HI
| valign="top" | Laughter
| valign="top" | W
| valign="top" | Watts
|-----
| valign="top" | HR
| valign="top" | Hear
| valign="top" | WA
| valign="top" | Word After
|-----
| valign="top" | HV
| valign="top" | Have
| valign="top" | WD
| valign="top" | Word
|-----
| valign="top" | HW 
| valign="top" | How, Copy?
| valign="top" | WDS
| valign="top" | Words
|-----
| valign="top" | IMI
| valign="top" | Repeat, say again
| valign="top" | WKD
| valign="top" | Worked
|-----
| valign="top" | LNG
| valign="top" | long
| valign="top" | WKG
| valign="top" | Working
|-----
| valign="top" | LTR
| valign="top" | Later
| valign="top" | WPM
| valign="top" | Words per minute
|-----
| valign="top" | LVG
| valign="top" | Leaving
| valign="top" | WRD
| valign="top" | Word
|-----
| valign="top" | MA & MILLS
| valign="top" | Milliamperes
| valign="top" | WX
| valign="top" | Weather
|-----
| valign="top" | MSG
| valign="top" | Message
| valign="top" | TXVR
| valign="top" | Transceiver
|-----
| valign="top" | N
| valign="top" | No, Nine
| valign="top" | XMTR
| valign="top" | Transmitter
|-----
| valign="top" | NCS
| valign="top" | Net Control Station
| valign="top" | XTL
| valign="top" | Crystal
|-----
| valign="top" | ND
| valign="top" | Nothing Doing
| valign="top" | XYL, YF
| valign="top" | Wife
|-----
| valign="top" | NM
| valign="top" | No More
| valign="top" | YL
| valign="top" | Young Lady
|-----
| valign="top" | NR
| valign="top" | Number
| valign="top" | YR
| valign="top" | Year
|-----
| valign="top" | NW
| valign="top" | Now , Resume transmission
| valign="top" | 73
| valign="top" | Best Regards
|
|}

In 1859, Western Union standardized on the "92 code", a series of telegraphic abbreviations in which numbers (originally 1 to 92) were assigned meanings.<sup>[http://www.napasars.org/news/sep07/92code.htm] (dead link, [https://web.archive.org/web/20080905172218/http://www.napasars.org/news/sep07/92code.htm archive])</sup> These were later included as part of the "Philips Code", a series of abbreviations first published in 1879 by Walter Phillips of the Associated Press for use in the telegraphic transmission of press dispatches.<sup>[http://www.radions.net/philcode.htm]</sup>

While most of the codes have fallen into disuse, the form 19 and 31 train orders remained in railroad use long beyond the end of landline telegraphy, the use of '30' at the end of a news wire story was continued through the teletypewriter era and the '73' and '88' greetings remain in use in amateur radiotelegraphy.

{| class="wikitable" style="line-height: 115%;" summary="Western Union codes"
|----- align="left"
! width="50" align="left"| Abbreviation
! Meaning
! Abbreviation
! Meaning
|+ '''Western Union codes'''
|-----
|'''1'''
|Wait a minute.
|'''25'''
|Busy on another wire.
|-
|'''2'''	
|Very Important.	                        
|'''26'''	
|Put on ground wire.
|-
|'''3'''	
|What time is it?	                
|'''27'''	
|Priority, very important.
|-
|'''4'''	
|Where shall I go ahead?	                
|'''28'''	
|Do you get my writing?.
|-
|'''5'''	
|Have you business for me?	        
|'''29'''	
|Private, deliver in sealed envelope.
|-
|'''6'''	
|I am ready.	                        
|'''30'''	
|No more - the end.
|-
|'''7'''	
|Are you ready?	                        
|'''31'''	
|Form 31 (permissive) train order.
|-
|'''8'''	
|Close your key, stop breaking.	        
|'''32'''	
|I understand that I am to ....
|-
|'''9'''	
|Priority business. Wire Chief's call.	
|'''33'''	
|Answer is paid.
|-
|'''10'''	
|Keep this circuit closed.	        
|'''34'''	
|Message for all officers.
|-
|'''12'''	
|Do you understand?	                
|'''35'''	
|You may use my signal to answer this.
|-
|'''13'''	
|I understand.	                        
|'''37'''	
|Inform all interested.
|-
|'''14'''	
|What is the weather?	                
|'''39'''	
|Important, with priority on through wire.
|-
|'''15'''	
|For you and others to copy.	        
|'''44'''	
|Answer promptly by wire.
|-
|'''17'''	
|Lightning here.	                        
|'''55'''	
|Important.
|-
|'''18'''	
|What's the trouble?	                
|'''73'''	
|Best Regards.
|-
|'''19'''	
|Form 19 (absolute) train order.	                
|'''77'''	
|I have a message for you.
|-
|'''21'''	
|Stop for meal.	                        
|'''88'''	
|Love and kisses.
|-
|'''22'''	
|Wire test.	                        
|'''91'''	
|Superintendent's signal.
|-
|'''23'''	
|All stations copy.	                
|'''92'''	
|Deliver Promptly.
|-
|'''24'''	
|Repeat this back.	                
|'''134'''	
|Who is at the key?
|-
|}

Two non-standard codes, rarely-used, were coined within the amateur radiotelegraph service. The [[YL|Young Ladies Radio League]] (YLRL) organized in 1939 and quickly coined '33' as "Love sealed with mutual respect and friendship between one [[YL]] and another YL".<sup>[http://www.repeater.org/links/links2/73.htm#33]</sup> More recently, '72' has been used in [[QRP]] operation to signify a '73' sent with reduced transmitter power.

== See also ==

* [[Calculators]]

{{operation}}
