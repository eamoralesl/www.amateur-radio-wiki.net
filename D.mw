{{glossary}}


==D==

'''DAC''' : Also known as D/A and D-to-A. Digital to Analogue converter. A device that converts digital signals into analouge signals.

'''dB''' : [[Decibels |Decibel]] - the ratio of two power measurements.

'''dBd''' : dB relative to a dipole. Also known as [[Gain#Gain_compared_to_a_half_wave_dipole_-_dBD |dBD]]

'''[[Gain#Gain_compared_to_an_isotropic_readiator_.3D_dBi |dBi]]''' : dB relative to a theoretical isotropic (point) source.

'''[[Radiated Power Measurement | dBm]]''': The power compared to a 1 milliwatt (1mW) source, expressed in decibels (dB) 

'''dBo''' : Optical gain. An unofficial term used by some amateurs working with transmission of data using light.

'''dBuv''' : a measure of voltage compared to one microvolt. 0dBuv = one microvolt.

''' De or DE''' : "this is" or "from" - for example an operator may use VL2TK de VK4ZW .......

'''DF''' : Direction finding. Also known as [[ARDF]] and "fox hunting", the use of triangulation and directional antennas to find a hidden transmitter. May be done recreationally, or as a means of locating the source of problematic radio frequency interference.

'''Diplexer''' : A frequency splitting device used to couple two transceivers to either a single antenna or a dual band antenna.

'''[[Dipole]]''' : An antenna with two collinear elements, usually of equal length, with feedpoint in the centre. Commonly used as the driven elements for more complex antennas such as the yagi or log-periodic, a dipole on its own is omnidirectional if mounted for [[vertical polarisation]] but has a figure-8 directional pattern if mounted horizontally.

'''[[Dish or Parabola | Dish]]''' : A highly directional [[Antennas|antenna]], parabolic in shape. Often used at [[Microwave and other bands |microwave]] frequencies.

'''[http://www.qsl.net/zl1bpu/DOMINO/Index.htm Domino EX]''' : a digital [[Modes | mode]] that uses a variant of MFSK known as IFK for transmission of information.

'''Downlink''' : The channel or [[Frequendy_Wavelength_and_Period |frequency]] used for [[Satellites |satellite]] to earth communications.

'''DPSK''' : '''D'''ifferential '''P'''hase '''S'''hift '''K'''eying - a form of BPSK

'''[http://www.drm.org/ DRM]''' : Digital Radio Mondiale. A system of digital broadcasting developed by a consortium of manufacturers, researchers, broadcasters and governments.

'''DSP''' : '''D'''igital '''S'''ignal '''P'''rocessing. The digital processing of signals in filtering, noise reduction etc.

'''[[DTMF]]''' Dual Tone Multi Frequency - the allocation of a unique tone pair to each button on an appliance (made up of two frequencies - high and low) that allows a computer to recognize the tone. Originally used on a wide scale in landline telephony to allow tone dialling using a small keypad on a telephone or handset, but has been used in amateur radio to remotely control repeaters, autopatch, IRLP or Echolink nodes.

'''Dual Band Antenna''' : An antenna designed to be used on two amateur [[Bands]]. 

'''[[Dummy load |Dummy Load]]''' : An artificial [[Antennas |antenna]] that does not radiate. A non-inductive power resistor, a dummy load is connected in place of the transmitting antenna and used when aligning transceivers.

'''Duplex''' : Transmit and receive are on two different frequencies - often use in repeaters with a shift (difference) of 600Hz.

'''Duplexer''' : A device that allows an antenna to transmit and receive simultaneously.

'''DVM''' : Digital voltmeter.

'''[[DXCC]]''' : DX Century Club

'''DX''' : Distance or distant station. Originally "distant exchange", from landline telephony. On HF radio, normally used to refer to a station on another continent or in an exotic location.

'''[[DXpedition]]''' : An expedition by amateurs to a location that may be geographically or physically remote and from where amateur contacts are rare.

'''Dynamic Range''' : How well a receiver can handle very strong signals wthout overloading.
