Related wiki pages [[Bands]], [[Propagation]] [[Antennas]]

{{Band
|band=60m
|USG=5.3305, 5.3465, 5.3665, 5.3715, 5.4035
|USE=5.3305, 5.3465, 5.3665, 5.3715, 5.4035
|USA=5.3305, 5.3465, 5.3665, 5.3715, 5.4035
|UST=None
|UK=Not used.
}}

The 60 metre band is an unusual [[HF]] band in that operation is limited to five specified channels, and the maximum power output allowed is defined by [[effective radiated power|ERP]] rather than input power.  It was created primarily to allow emergency communication during times when propagation conditions dictated use of a band higher frequency than 80 metres but lower than 40 metres.

The 60m Band is not available to hams in all countries, for example Australia. Its five USB channels exist on a secondary basis in the United States, with the NTIA (a US government agency) as the primary occupant of the band. It does not exist in Canada.

== Equipment ==

Because 60 metres was allocated relatively recently, most older amateur [[HF]] equipment does not support it.  However, nearly all new multiband [[HF]] equipment can operate on 60 metres. [[Elecraft]] offers 60m as an optional hardware upgrade.

== Modulation ==

Only Upper Sideband Modulation (USB) is permitted on the 60 metre band.  Unlike all other [[HF]] bands, [[CW]] operation is not allowed anywhere in the band.  Amateurs are secondary users, and must yield the band to the primary users on request.  Limiting amateur modulation to USB guarantees that the primary users can communicate with amateurs when necessary.

== Propagation ==
While propagation over long distances by nighttime skywave would in theory be possible at these frequencies, strict power limits (maximum power is fifty watts ERP), a limited number of available channels and the unavailability of this spectrum for many non-US hams may limit its use in international communication.

{{bands}}
