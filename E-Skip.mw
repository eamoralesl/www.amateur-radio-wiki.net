Related wiki pages : [[Propagation]], [[Meteor scatter]], [[Aurora]], [[Tropospheric ducting]]

==What is it?==

The E-layer is the lowest layer of the atmosphere and can typically be found at altitudes between about 80km and 100km. the E-Layer is characterised by high electron densities, and high temperatures (between 300K and 122K)

E-Skip propagation is the reflection of radio waves off the E-Layer, allowing reception many thousands of kilometres distant from normal reception areas.

E-Layer communications are characterised by:
* weaker but longer paths than usual
* directions for optimal communications, and hence target areas are predictable
* time frames for communications are predictable
* QSB (fading) is slow and predictable

=== At which frequencies does E-Skip occur? ===

E-Skip generally occurs at night in the medium and shortwave [[HF]] parts of the spectrum.

===Sporadic E-Skip ===

Sporadic E-skip is associated with scattered regions of especially dense ionisation that occur seasonally within the E-layer. generally it occurs:

* daily during the day in equatorial regions
* commonly in temperate regions in late spring, early summer and sometimes in early winter
* in association with auroras in Polar regions
* at frequencies up to about 150MHz
* can provide communication over distances up to 2,400Km

Sporadic E-Skip is characterised by:
* strong, relatively short paths
* unpredicatable times
* unpredictable directions
* fast and unpredictable QSB (fading) events

===Further reading===

* [http://www.amfmdx.net/propagation/Es.html Mid latitude sporadic E (ES) - A review]
* [http://en.wikipedia.org/wiki/TV_and_FM_DX#Sporadic_E_propagation_.28E-skip.29 Wikipedia article]

{{propagation}}
