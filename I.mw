{{glossary}}

==I==

'''[http://www.iaru.org/ IARU] ''' : '''I'''nternational '''A'''mateur '''R'''adio '''U'''nion.  An international society that works for and with national organisations to promote the interests of hams.

'''IF''' : '''I'''ntermediate '''F'''requency, at which signals are amplified and processed internally within a receiver.

'''IFK''' : '''I'''ncremental '''F'''requency '''K'''eying a variant of MFSK

'''Image frequency''' : A frequency separated from that of the desired received signal by twice the receiver's intermediate frequency. 

'''Ionosphere''' : a region in the atmosphere that contains ionised gasses that refract (bend) radio waves and direct them back to earth.

'''[[Inductors | Inductance]]''' : A measure of how well a coil stores energy in the form of a magnetic field.

'''[[Inductors | Inductor]]''' : a coil of wire. Inductors are often used with [[Capacitors | capacitors]] in tuned circuits such as [[Filters |filters]] and [[Oscillator Design |oscillators]]. Some inductors are wound around a core of metallic material. This affects their inductance.

'''[[Radio/PC Interfaces | Interface]]''': usually refers to a circuit used to connect the sound card of a computer to a transceiver to allow digital [[Modes | modes]] to be used.

'''Interference''' : The interaction between a desired frequency and an undesired frequency.

'''Intermodulation''' : The undesired mixing of two or more frequencies, producing sometimes undesirable additional frequencies.

'''Insulator''' : A substance through which electricity does not pass easily - known as a dielectric in [[Capacitors | capacitors]]

'''[[IRLP]]''' '''I'''nternet '''R'''adio '''L'''inking '''P'''roject. A method of cross-world linking of repeaters so that very basic equipment can be used to communicate with hams in other countries.

'''Inverter''' : a device that converts a DC source to and AC output.

'''[http://www.itu.int/net/home/index.aspx ITU]''' : International telecommunication Union. The international organisation set up to standardise and regulate radio communications.
