[[File:Small-Loop-Antenna-G4JUV.gif|right]] A '''small transmitting loop''', '''small tuned loop''' or '''magnetic antenna''' is defined as a [[loop antenna]] of less than one-tenth to one-quarter of a wavelength in circumference. These are normally constructed as a single turn of heavy-gauge wire (AWG #10 or larger), copper pipe or tubing and are tuned to one specific frequency at a time using a series variable [[capacitor]].

As these antennas can be constructed in relatively little space, often with diameter of several feet or less, they are used in [[HF]] applications where no suitable location is available for a full-sized antenna such as the [[beam antenna]]. In some cases, these have been constructed in attics or used for [[portable antenna|portable]] operation.<sup>[http://www.laud.no/la6nca/loop/]</sup>

While a circular loop is efficient in that it provides the largest area for any one given length of conductor, other form factors (such as a square formed from copper pipe and 90° elbows) have also been successfully employed.

== Theory of operation ==
A wire loop inherently acts as an [[inductor]] with a radiation resistance of a fraction of an ohm. In order to keep other [[resistance]] and [[impedance]]s from becoming predominant, the resistance must be kept to a minimum in transmitting antennas by the use of heavy wire or tubing and the inductance of the loop must be compensated using a large series capacitor.

This yields an antenna which operates as a resonant [[LC circuit]] at one specific frequency with an impedance very close to zero; at other frequencies, impedance rises sharply and the antenna is unresponsive. 

== Tuning ==
Due to the need to maintain low impedance, capacitors used must have effective series resistance of near-zero, despite their handling of large amounts of current and RF voltages which (in any larger than a QRP operation) often reach 4kV or more. As the circuit is largely built from reactive elements, voltages and currents at individual points far exceed those which normally would be associated with the real power being transmitted. The capacitors must also be variable over a wide range to allow the loop to be manually tuned to one of various frequencies and bands in the HF spectrum.

Vacuum variable capacitors are very well suited to this task, but may be costly even from industrial-surplus sources. A possible alternative is an air-variable capacitor in which all connections to individual plates are welded and there are no moving connections to the rotor. The ''butterfly capacitor'' ([http://www.mfjenterprises.com/Product.php?productid=MFJ-19 MFJ-19], [http://www.mfjenterprises.com/Product.php?productid=MFJ-23 MFJ-23]) is a split-stator capacitor based on this approach; an air-variable design, it effectively has two stators (one connected to each end of the loop) and a rotor which moves between them to adjust capacitance. No direct electrical connection is wired to the rotor; there are no brushes or slip rings.

Some radioamateurs have constructed their own capacitors for use in tuned loop antennas; these are usually air-variable capacitors built from individually-machined plates or trombone capacitors in which two concentric pieces of copper pipe are separated by an insulator and one slides into the other to vary the capacitance.

As the circuit is very frequency-selective, often a means is desired to remotely adjust capacitance to tune the antenna to the desired channel frequency. Reduction drives may be used to convert a single-turn variable capacitor to more finely-tunable multiturn control and motors (such as steppers designed for computer printers or servos used in radio-control operation) may provide a means to tune the antenna remotely.

== Feedpoint matching ==
As the impedance of the small loop antenna at resonance is near-zero, it must be matched to the higher impedance (typically 50&Omega;) of the transmitting equipment.

Possible methods to match transmitter to loop are:
* Matching loop - A second loop, typically no more than one-fifth the size of the main loop, is constructed and installed adjacent to one edge of the main loop and used to couple the signal inductively.<sup>[http://www.qsl.net/ea5xq/ea5xqpre_magneticloop.html]</sup>
* Delta match - One small portion of the loop is used as an [[autotransformer]] by connecting two taps to one portion of the loop circumference, usually at the point most distant from the capacitor. These are then connected using balanced line (two conductors of equal length and size) to a [[balun]] or matching device and fed as a balanced load.
* Army match - The transmitter is matched to the antenna capacitively
* Gamma match - The shield of an unbalanced line is connected directly to the circumference of the loop at a point directly opposite the tuning capacitor. The centre conductor is then installed, unshielded, at a 1" spacing from the loop and hard-wired to the loop at a more distant point - effectively forming an unbalanced matching loop which includes part of the main loop as one side of the feed loop.<sup>[http://www.qsl.net/dl7jv/e.mag.htm]</sup>

== External links ==
* [http://www.qsl.net/we6w/projects/160_loop.txt 160m Short TX Loop] antenna, WE6W
* [http://web.telia.com/~u85920178/antennas/frameant.htm 80 metre Frame Antenna] and [http://web.telia.com/~u85920178/antennas/servo.htm Remote Antenna Tuner], Harry Lythall - SM0VPO
* [http://frrl.wordpress.com/2009/03/21/limited-space-antennas-the-small-transmitting-loop-antenna/ The AEA Isoloop]
* [http://qrpfr.free.fr/?2006/04/14/20-antenne-cadre-par-f5ngz Antenne Cadre] par F5NGZ + Description F5TZ, QRP FR (en français)
* [http://www.qsl.net/dj3tz/loop2.html DJ3TZ's Small Tuned Loop Antenna] Page
* [http://www.davesergeant.com/loops.htm Experiences with Loop antennas] at G3YMC
* [http://www.qsl.net/hb9mtn/hb9mtn_magnet_80.html Magnetic Loop for 80m - 40m], HB9MTN
* [http://www.qsl.net/g4fon/MagLoop.htm Magnetic Loop Antenna], G4FON
* [http://www.laud.no/la6nca/loop/ Magnetic Loop Antenna], LA6NCA Larvik, Norway
* [http://www.qsl.net/mnqrp/Loop/Mag_Loops.htm Magnetic Loop Antennas], Tony Van Herck ON4CEQ 
* [http://home.alphalink.com.au/~parkerp/gateway/nodec97.htm A magnetic loop antenna for HF], Peter Parker VK3YE
* [http://www.kr1st.com/magloop.htm My Magnetic Loop Antenna], KR1ST - a small transmitting loop
* See also [http://www.kr1st.com/swlloop.htm A small receiving loop with a good frequency range]
* [http://www.geocities.com/gw0tqm/magloop/magloop.htm The Myth and Mystery of the Magnetic Loop], GW0TQM
* [http://homepages.ipact.nl/~geurink/magneticloop/ PA3CQR Magnetic loop antenna]
* [http://dt.prohosting.com/hacks/antenna/magloop.html Portable HF Transmitting Loop Antenna], N5IZU
* [http://www.qsl.net/pa3hbb/magloop2.htm Practical Experiments with Magnetic Loop antennas], David Reid PA3HBB / G0BZF
* [http://users.tpg.com.au/users/ldbutler/HFTXLoop.htm A Small Transmitting Loop Antenna for 14MHz and 21MHz], Lloyd Butler VK5BR, Amateur Radio, November 1991.
* [http://www.aa5tb.com/loop.html Small Transmitting Loop Antennas], AA5TB (includes aa5tb_loop_v1.22a.xls small loop spreadsheet calculator)
* [http://www.g4ilo.com/stealth.html Stealth Amateur Radio], G4ILO
* [http://www.qsl.net/ok1fou/e_loop10.html An Unusual Two Band Magnetic Loop Antenna] (for 14 and 10 MHz), OK1FOU
* [http://www.standpipe.com/w2bri/article1.htm W2BRI's Magnetic Loops]

{{antennas}}
