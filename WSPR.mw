Related wiki pages [[Repeater listings]], [[APRS]], [[D-Star]], [[Echolink]], [[IRLP]], [[Packet]], [[Slow-Scan Television (SSTV)]], [[Fast-Scan Television (ATV)]], [[Optical communications]], [[Software]], [[WSJT]]


== WSPR '''W'''eak '''S'''ignal '''P'''ropagation '''R'''eporter ==

WSPR (usually pronounced "whisper") is a software application that uses the transmission mode MEPT-JT (Manned Experimental Propagation Transmitter - Joe Taylor).

In MEPT transmissions, the radio becomes a beacon that transmits for just under 2 minutes, transmitting callsign, locator and power information.  Modulation is by narrow band [http://en.wikipedia.org/wiki/Frequency-shift_keying FSK].

== Frequencies in use for WSPR ==

<table border="2" cellpadding="2" cellspacing="2" width="50%">
<tr>
<th style="text-align: center">Band</th>
<th style="text-align: center">Dial freq (MHz)</th>
<th style="text-align: center">Tx freq (MHz)<br /> </th>		
</tr>
<tr>
<td style="text-align: center"> 160m</td>			
<td style="text-align: center"> 1.836600 </td>			
<td style="text-align: center"> 1.838000 - 1.838200 </td>		
</tr>
<tr>
<td style="text-align: center"> 80m<br /> </td>			
<td style="text-align: center"> 3.592600 </td>			
<td style="text-align: center"> 3.594000 - 3.594200<br /> </td>		
</tr>
<tr>
<td style="text-align: center"> 60m<br /></td>			
<td style="text-align: center"> 5.287200 </td>			
<td style="text-align: center"> 5.288600 - 5.288800 </td>		
</tr>
<tr>
<td style="text-align: center"> 40m<br /> </td>			
<td style="text-align: center"> 7.038600 </td>			
<td style="text-align: center"> 7.040000 - 7.040200 </td>		
</tr>
<tr>
<td style="text-align: center"> 30m<br /> </td>			
<td style="text-align: center"> 10.138700 </td>			
<td style="text-align: center"> 10.140100 - 10.140300<br /> </td>		
</tr>
<tr>
<td style="text-align: center"> 20m<br /> </td>			
<td style="text-align: center"> 14.095600 </td>			
<td style="text-align: center"> 14.097000 - 14.097200<br /></td>		
</tr>
<tr>
<td style="text-align: center"> 17m<br /></td>			
<td style="text-align: center"> 18.104600</td>			
<td style="text-align: center"> 18.106000 - 18.106200<br /></td>		
</tr>
<tr>
<td style="text-align: center"> 15m<br /></td>			
<td style="text-align: center"> 21.094600</td>			
<td style="text-align: center"> 21.096000 - 21.096200<br /></td>		
</tr>
<tr>
<td style="text-align: center"> 12m<br /></td>			
<td style="text-align: center"> 24.924600<br /></td>			
<td style="text-align: center"> 24.926000 - 24.926200<br /></td>		
</tr>
<tr>
<td style="text-align: center"> 10m<br /></td>			
<td style="text-align: center"> 28.124600<br /></td>			
<td style="text-align: center"> 28.126000 - 28.126200<br /> </td>		
</tr>
<tr>
<td style="text-align: center"> 6m<br /> </td>			
<td style="text-align: center"> 50.293000<br /></td>			
<td style="text-align: center"> 50.294400 - 50.294600 </td>		
</tr>
<tr>
<td style="text-align: center"> 2m<br /> </td>			
<td style="text-align: center"> 144.488500<br /> </td>			
<td style="text-align: center"> 144.489900 - 144.490100	</td>		
</tr>
</table>

==WSPR interface==

The WSPR interface has three main panes:
* Top left - the activity pane which is divided into vertical strips each representing a time of 2 minutes. In the example below, transmits are shown as green vertical lines.
* Top Tight - the active stations pane which shows which stations have been received.
* Bottom Right - received station details, showing the receive parameters of active stations.

[[Image:VK4YEH-WSPR.jpg |700px]]

One feature of this receive pane is the multiple receive tracks from the same station (VK4YEH) at the same time. Larry Weaver supplied this explanation:

"''The cause of receiving stations reporting two instances of the same transmission is usually caused by local QRM at the receive location or over driving of the sound card input from the receiver, or both. I, along with others, have had our transmissions reported in doubles at various times. It is usually a new WSPR user who has not adjusted his input level properly or strong QRM during a WSPR receive cycle.'' "

==WSPR Spots==

[http://wsprnet.org/meptspots.php  This link] goes to the WSPR spots page. WSPR receivers are linked via the internet, and information is automatically uploaded to the server by the WSPR software ( this can be turned off ).  An example of the spots page is below:

[[Image: VK4YEH WSPR spots.png |700px]]

The fact that stations are linked means that time synchronisation is critical. Reports suggest that time differences of greater than 2 seconds will result in errors.  Many operators use [http://www.thinkman.com/dimension4/ Dimension 4] to synchronise their Windows PC.

==Time Synchronisation==

Effective use of WSPR is dependent on accurate synchronisation of the computer clock to "internet time".

While this can be achieved by using the "Date and Time properties" function in windoes, some users have found [[http://www.thinkman.com/dimension4/ Dimension 4]] to be a very useful utility.

== External links ==
* [http://physics.princeton.edu/pulsar/K1JT/ WSJT home page]
* [http://www.g4ilo.com/wspr.html G4ILO] a good intro to WSPR.
* [http://wsprnet.org/drupal/node WSPRnet.org] A great forum for information, chat and spots.
* [http://groups.yahoo.com/group/wspr_mept/ wspr_mept] yahoo user group for WSPR


{{software}}
