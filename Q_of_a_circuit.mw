==What is '''Q'''==

The '''Q''' or ''Quality factor'' of a resonant circuit is the ratio of stored power to dissipated power in the [[Reactance]] and [[Resistance]] of the circuit.

Generally speaking, a higher '''Q''' corresponds to a narrower bandwidth.

==How is '''Q'''  calculated?==



Note that at resonance, a series circuit "appears" to be purely resistive (it behaves like a resistor). Below resonance it appears to be capacitive (it behaves like a capacitor), and above resonance it appears to be inductive (behaves like an inductor)

<math>Q=\frac{P_{stored}}{P_{dissipated}}=\frac{I^2X}{I^2R}=\frac{X}{R}</math>  where:

* X = capacitive or inductive reactance at resonance
* R = series resistance
* P = Power
* I = Current

This formula is for all series resonant circuits and also works for parallel resonant circuits in which a small resistor is in series with the inductor.

If there is a large resistor in parallel with both the inductor and the capacitor, the formula becomes:

<math>Q=\frac{P_{dissipated}}{P_{stored}}=\frac{I^2R}{I^2X}=\frac{R}{X}</math>

==Admittance==

For a given circuit element , the admittance is the reciprocal of the impedance.

Admittance is most useful in parallel AC circuit calculations where there are no series components. The equivalent admittance of a parallel circuit is the sum of the admittances of the components. 



{{electronics}}
