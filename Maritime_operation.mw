== Maritime mobile ==
Amateur radio stations have been installed on ships at sea, both for conventional HF operation and for digital modes such as [[Winlink]]. In some countries, a specific block of callsigns is reserved for ocean-going vessels (for instance, VE0 is a Canadian ship at sea).

In [[contesting]], operation from a ship docked or at anchor off the shores of a distant island typically does not qualify a station as being on the island; all or some key part of the station (such as the transmitter or antenna) must be deployed ashore.

== Lighthouses and coastal stations ==
Historically, marine radio coastal stations had often located at or near manned lighthouses to ensure communication with ships at sea. In many cases, the lighthouse itself would be in a remote or isolated location where radio communication would be highly desired as a means to allow those operating the beacon to remain in touch with the mainland. While most modern lighthouses now are automated for unattended operation, the associated ship-to-shore radio installations in many cases have been rendered obsolete. 

Where the original installations were of historical significance (perhaps due to a key location or a historical tie as distant as the Marconi radio-telegraph era of a century ago) a radio amateur station will often be deployed temporarily or permanently in commemoration of the original maritime installation. The 500kc marine radiotelegraph service, now largely abandoned but still of historical importance, is often commemorated with museums and radio clubs using HF amateur radio to provide an operating CW radio telegraph station.

[[International Events | International Marconi Day IMD]], an international annual weekend commemoration promoted by the Cornish Amateur Radio Club for the birthday of Guglielmo Marconi, takes place in late April; stations have also been deployed April 14-15 to commemorate radio communication with the sinking RMS Titanic in 1912.

* Cape Race on the Dominion of Newfoundland's Avalon Peninsula was long the first point of contact for ships arriving from Europe; its coastal marine radio station (originally Marconi station CE, MCE, VCE) was closed in the 1960's but is now commemorated as museum station VO1MCE<sup>[http://www.weyvalleyarg.org.uk/pages/xtras/weyaheads/weyahead_titanic/weyahead_titanic.html], [http://www.gb4imd.org.uk/cape_race.htm]</sup> by the Irish Loop Amateur Radio Club.<sup>[http://www.qrz.com/db/VO1MCE]</sup>
* A former Marconi station established 1913 at Bolinas and Point Reyes National Seashore, in California, had operated as RCA marine coastal station KPH. No longer an operating marine ship-to-shore coastal station, the facilities have instead hosted commemorative radio amateur station K6KPH.<sup>[http://www.southgatearc.org/news/apr2005/imd.htm]</sup>

An [[International Events | IILW - International Lighthouse/Lightship Weekend]] held annually in mid-August<sup>[http://illw.net], [http://vk2ce.com/illw/]</sup> attracts stations at or adjacent to over 400 lighthouses in 50 countries worldwide; the event originated in Scotland in 1999. The US-based [http://www.arlhs.com Amateur Radio Lighthouse Society]<sup>[http://www.arrl.org/news/features/2004/06/25/1/]</sup> maintains a [http://wlol.arlhs.com World List of Lights] and holds an annual [http://arlhsconvention.com convention]. The [http://www.barls-gb.supanet.com British Amateur Radio Lighthouse Society] offers awards for contacts with ten or more lighthouses, operation from ten or more lighthouses, contact with BARLS members operating from ten or more overseas lighthouses and contact with 25 or more BARLS member stations. A lighthouse site may also be a tempting [[DXpedition]] target as many are situated on island or remote locations which would otherwise be largely uninhabited.<sup>[http://www.stpaulisland.net]</sup>

== Museum ships ==
As historic ships (including many which had served during wartime) were removed from service after many years of seafaring, a few have been opened to the public and converted to maritime museums. These ships had historically used medium-wave or HF radio for communications (with 500kHz CW, 2182kHz voice as the most common calling frequencies) and efforts are made to restore both ship's machinery and the radio room to operating condition. Restoration of the original or vintage radio equipment for use on the radioamateur bands may be the most viable way to get a working shipboard CW station back on the air as the original 500kHz marine radiotelegraph band has largely fallen into disuse.

In some cases, temporary amateur stations have been deployed aboard museum ships during special events. An annual Museum Ships Weekend puts museum ships on the air, complete with commemorative [[QSL]] cards and a certificate for amateur stations which successfully contact fifteen or more unique museum ships during the two-day event.

* [http://www.nj2bb.org/museum/index.html Museum Ships Weekend Event], NJ2BB
* [http://w0oog.50megs.com/ Submarine Veterans Amateur Radio]
* [http://www.marinefunker.de/eng/shiplist.html International List of Museum ships fitted with Amateur Radio]
* [http://www.eham.net/articles/21695 Anchors Away for Historic Ships Ham Radio Event], Don Keith N4KC, May 30, 2009
* [http://jproc.ca/radiostor/ssjbrown.html MY DAY ABOARD THE  S.S. JOHN BROWN], Jerry Proc VE3FAB
* [http://www.lsu.edu/brarc/uss_kidd/Museum_ships.htm Museum Ships on the air], W5KID (USS Kidd)
* [http://www.qrz.com/db/WW2SUB USS Batfish Memorial Station] (Oklahoma), WW2SUB

== Islands ==
While many of the most rare entities in contests such as [[DXCC]] tend to be remote or uninhabited islands, there are also a number of specialised awards in which all of the available entities represent individual islands or island groups.

=== IOTA ===
Islands On The Air (IOTA) is a DX award programme created 30 years ago by Geoff Watts and administered since 1985 by RSGB, the Radio Society of Great Britain. Entities to be collected normally represent groups of islands (for instance, EU-005 is Great Britain) in sea or ocean locations. Islands on inland waterways (such as Montréal or the 1000 Islands in the freshwater St. Lawrence Seaway region) do not qualify. IOTA nominally operates worldwide.<sup>[http://www.eskimo.com/~oolon/wwdxc/iota3.htm]</sup>

=== National island awards ===
While individual national awards may differ from one country to another, common characteristics distinguishing national awards from the IOTA programme include:
* Entities for national awards tend to be individual islands, instead of the island groups used by IOTA. While IOTA's numbering is continent + sequential number (such as NA001 for the first group listed in North America), national awards typically number with a state or province abbreviation + sequential number (such as QC001, Québec - Anticosti) for each island.
* Contacts (QSO's) to two islands in the same IOTA group typically count as two separate entities for national contesting, but as just one entity for the IOTA awards.
* Inland islands (on lakes or freshwater rivers) are not listed and do not qualify for IOTA; national awards will normally allow named islands  in both freshwater and saltwater environments.
* IOTA's list of island groups, while worldwide in scope, is a closed list in that one cannot easily request a missing island or group be added; national programs such as CIsA (Canada) or USI (United States) often allow additional islands to be readily qualified upon meeting specific criteria (such as size, number of contacts made, identification by name on local maps)

A few examples include:
;Canada: Canadian Islands Award (CIsA) a national island awards program begun in 1973 by Garry V. Hammond VE3GCO and administered by the Maple Leaf Radio Society.<sup>[http://www.qsl.net/ve3tpz/cisa/]</sup>
;Italy: Italian Islands Award<sup>[http://www.425dxn.org/iia/index.html]</sup>
;Japan:Japanese IOTA Islands Award (JIAA)<sup>[http://www3.ocn.ne.jp/~iota/newpage61.htm]</sup> for QSO's with more than 10 Japanese Islands, additional levels awarded for 100, 200, 300 or 400 contacts.
;Portugal: Portuguese Islands Plaque (Placa Ilhas Portuguesas)<sup>[http://www.qsl.net/odxg/pip_award_english.htm]</sup> is awarded by the OESTE DX GANG association for HF contacts to at least 25 radio amateurs on different Portuguese islands
;United Kingdom: English Islands Award<sup>[http://26dx047.net/eia/]</sup> currently recognises 408 islands in 21 UK counties; Worked All Britain also offers an Islands Award<sup>[http://wab.intermip.net/Islands%20Award.php]</sup>
;United States: United States Islands awards programme (USi) started in summer of 1994 by John KL7JR to cover saltwater and inland fresh water river or lake islands of the fifty US states and US territories or protectorates (including Hawaii and the US Virgin Islands).<sup>[http://usislands.org]</sup> An annual US Islands W/VE QSO Party invites amateurs to make contacts from (or to) the various USI or CIsA islands; both are "open-ended" in that individual islands are added to the US or Canadian lists when a minimum of twenty-five contacts have been made from the island with amateur radio stations in at least two countries.<sup>[http://www.contesting.com/articles/717]</sup>

Many additional national islands DX award programmes are listed [http://webhome.idirect.com/~va3rj/isl_awards.html here].

== External links ==
* Lists of lighthouse-related radio sites [http://webhome.idirect.com/~va3rj/light_links.html] [http://www.ringsurf.com/ring/arlh/]

{{operation}}
