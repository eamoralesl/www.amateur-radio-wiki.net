A [[time]] beacon is a special type of [[Beacons|beacon]] that transmits [[time]] data on a continuous basis. Time data is derived from atomic clocks - clocks that maintain an accuracy to within one billionth of a second per day. A number of time scales are generated, including UTC (Coordinated Universal Time) often of most use to radio amateurs.

== Use for Amateurs ==
* Calibrating local VFO frequencies for receivers and transmitters (WWV is an exact, precise 10MHz reference frequency)
* Aligning [[SSTV]] software to computer clock speed so as to reduce/eliminate [[slant]] on received pictures.

== Time beacon facilities ==

=== Canada ===
* [http://inms-ienm.nrc-cnrc.gc.ca/time_services/shortwave_broadcasts_e.html CHU] in Canada (3.330MHz, 7.85MHz, 14.670 MHz)

===  China ===

* BPC  in China (68.5kHz)

=== France ===

* [http://en.wikipedia.org/wiki/T%C3%A9l%C3%A9_Distribution_Fran%C3%A7aise TDF] in France (162kHz)

=== Germany ===
* [http://www.ptb.de/en/org/4/44/442/dcf77_1_e.htm DCF77] in Germany (77.5kHz)

===Japan===

* [http://en.wikipedia.org/wiki/JJY JJY] in Japan (Frequency 40kHz and 60kHz)

=== Switzerland ===
* [http://www.metas.ch/root_legnet/Web/Fachbereiche/Zeit_Frequenz/dissemination/HBG HBG] Switzerland (75kHz)

===United Kingdom ===

* [http://en.wikipedia.org/wiki/MSF_time_signal MSF] in the United Kingdom (60kHz)

=== USA ===

* [http://www.nist.gov/ NIST] (National Institute of Standards and Technlogy); WWV in Fort Collins, Colorado and WWVH in Hawaii USA. (2.5 MHz, 5MHz, 10MHz, 15MHz, 20MHz)
* NIST also operates WWVB (60kHz) from the Colorado location only

== Related pages ==

* [[Bands]]
* [[Beacons]]
* [[Time]]

{{operation}}
