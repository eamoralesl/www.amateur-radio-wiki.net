Impedance Matching is required to '''maximise energy transfer''' from an AC source to a device.

<u>Examples</u>: 
* Transform 240V AC to 12V AC for a power supply.
* Transform a 7 Kohm audio source (from an amplifier for instance) down to 16 Ohms for a headphone or loudspeaker.
  - both of these can be done by using a transformer.

* Couple an antenna with impedance other than 50 Ohm and non-zero reactance to a transceiver with 50 Ohm purely reactive impedance. 
This is done by an ATU. The simplest form of an ATU consists of a variable capacitor between TRTX and ground followed by a series variable inductor, which in turn is followed by a series variable capacitor which leads to the antenna (in case of an unbalanced feed line).

[[File:ATU.jpg]]


{{antennas}}
