'''Related Wiki Pages''' [[Awards and Certificates]], [[Clubs]], [[DXCC#QRP_DXCC|QRP DXCC]], [[Modes]], [[Bands]]

QRP is a [[Modes#Continuous Wave(CW)|CW]] prosign that means "low power".  It is commonly agreed that QRP refers to power of 5 watts or less.  (The term QRPp is used for power less than 1 watt.)

The number of amateur operators engaging in QRP is growing.  QRP rigs are ideal for [[Trail-Friendly Radio|field use]], and they are easy to build as [[kits#HF Transceiver Kit Sources|kits]] or [[homebrew]].  Because no high-power output transistors are needed in the final transmitter stage, they are inexpensive and safe for building, modification, and experimentation.
Because they draw little power, small batteries or solar cells are sufficient to power them.

==Common QRP frequencies==

<table border="2" cellpadding="2" cellspacing="2" width="50%">
<tr><th>Band Name</th><th>Calling Frequency</th><th>Mode</th></tr>
<tr><td>160 Meters</td><td>1810 kHz</td><td>CW</td></tr>
<tr><td>160 Meters</td><td>1818 kHz</td><td>CW</td></tr>
<tr><td>160 Meters</td><td>1843 kHz</td><td>SSB Europe</td></tr>
<tr><td>160 Meters</td><td>1910 kHz</td><td>LSB</td></tr>
<tr><td>80 Meters</td><td>3560 kHz</td><td>CW</td></tr>
<tr><td>80 Meters</td><td>3690 kHz</td><td>SSB Europe</td></tr>
<tr><td>80 Meters</td><td>3710 kHz (Novice in USA)</td><td>CW</td></tr>
<tr><td>80 Meters</td><td>3711 kHz (Novice in USA)</td><td>CW</td></tr>
<tr><td>75 Meters</td><td>3985 kHz</td><td>LSB</td></tr>
<tr><td>40 Meters</td><td>7040 kHz</td><td>CW</td></tr>
<tr><td>40 Meters</td><td>7090 kHz</td><td>SSB Europe</td></tr>
<tr><td>40 Meters</td><td>7110 kHz (Novice in USA)</td><td>CW</td></tr>
<tr><td>40 Meters</td><td>7286 kHz</td><td>LSB</td></tr>
<tr><td>30 Meters</td><td>10106 kHz</td><td>CW</td></tr>
<tr><td>30 Meters</td><td>10116 kHz</td><td>CW</td></tr>
<tr><td>20 Meters</td><td>14060 kHz</td><td>CW</td></tr>
<tr><td>20 Meters</td><td>14285 kHz</td><td>USB</td></tr>
<tr><td>17 Meters</td><td>18069 kHz</td><td>CW</td></tr>
<tr><td>17 Meters</td><td>18096 kHz</td><td>CW</td></tr>
<tr><td>17 Meters</td><td>18130 kHz</td><td>USB</td></tr>
<tr><td>15 Meters</td><td>21060 kHz</td><td>CW</td></tr>
<tr><td>15 Meters</td><td>21110 kHz (Novice in USA)</td><td>CW</td></tr>
<tr><td>15 Meters</td><td>21285 kHz</td><td>SSB Europe</td></tr>
<tr><td>15 Meters</td><td>21385 kHz</td><td>USB</td></tr>
<tr><td>12 Meters</td><td>24906 kHz</td><td>CW</td></tr>
<tr><td>12 Meters</td><td>24956 kHz</td><td>USB</td></tr>
<tr><td>10 Meters</td><td>28060 kHz</td><td>CW</td></tr>
<tr><td>10 Meters</td><td>28110 kHz (Novice in USA)</td><td>CW</td></tr>
<tr><td>10 Meters</td><td>28360 kHz</td><td>SSB Europe</td></tr>
<tr><td>10 Meters</td><td>28885 kHz</td><td>USB</td></tr>
<tr><td>2 Meters</td><td>144060 kHz</td><td>CW</td></tr>
<tr><td>2 Meters</td><td>144285 kHz</td><td>SSB</td></tr>
</table>

== Links ==
* [http://home.alphalink.com.au/~parkerp/qrp.htm QRP Australia]
* [http://www.gqrp.com The G QRP Club]
* [http://home.alltel.net/johnshan/links_ss_qrpc.html QRP Clubs] International list of QRP clubs.
* [http://www.qrparci.org/ International QRP Club]


{{operation}}
