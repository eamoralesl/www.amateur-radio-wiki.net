Related wiki pages [[Bands]], [[Propagation]] [[Antennas]]

{{Band
|band=80m
|USG=3.525-3.600<br/>3.800-4.000
|USE=3.500-4.000
|USA=3.525-3.600<br/>3.700-4.000
|UST=3.525-3.600 (CW only)
|UK=3.5-3.8
}}

80 metres is primarily used for contacts within a local region.  Reliable communication over modest distance are a primary feature of the band.  Many regional [[net|nets]] meet on this band on a regular schedule.

The upper half of the 80 metre band (3.75 MHz - 4.00 MHz), which is used primarily for [[Modes#Single Sideband Modulation (SSB)|SSB]], is often known as '''75 metres'''.

== Equipment ==

Most multiband amateur [[HF]] equipment supports the 80 metre band.  Single-band 80 metre rigs are also available from [[Oak Hills Research]] and [[Small Wonder Labs]].

Because natural noise levels ([[QRN]]) are high on this band, [[QRP]] operations may be somewhat more difficult than on higher-frequency bands.  However, it is very easy to [[homebrew]] equipment for the band, as designs, components, and construction techniques are not critical.

== Modulation ==

Operation on the 80 metre band is primarily [[Modes#Continuous Wave (CW)|CW]] and [[Modes#Single Sideband Modulation (SSB)|SSB]], but operators also use modes appropriate for [[HF]] such as [[Modes#Amplitude Modulation (AM)|AM]], [[Modes#Radio Teletype (RTTY)|RTTY]] and [[Slow-Scan Television (SSTV)|SSTV]].

== Propagation ==

80 metres is primarily used for routine amateur contacts over distances of a few hundred kilometers, although longer-distance communication also occurs routinely.

Radio communication on this band suffers from [[Propagation#Propagation_Theory_.28unfinished.29|D-layer]] absorption during the day, although not as severely as on [[160 metres]].  Communication paths during daylight typically range up to a thousand kilometres.  Long-distance [[DX]] contacts are possible at night through [[refraction]] of signals via the [[Propagation#Propagation_Theory_.28unfinished.29|F2 layer]].

== Australian Bandplan==

Access :

3.500 -3.700 MHz  All licence classes 

3.776 - 3.800 MHz  Advanced licensees only  
 
[[Image:Vk4yeh_vk_80m_bandplan.jpg]]


{{bands}}
