I am [https://wiki.koumbit.net/TheAnarcat anarcat]. I just passed my basic qualitification in Canada, with honors (so I can operate HF), yay! I am now AKA VA2ANK. -- [[User:TheAnarcat|TheAnarcat]] 21:50, 10 October 2010 (CDT)

= My rig =

* Transceiver:
** Yaesu FT-100D - [http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&item=180565016542&ssPageName=STRK:MEWNX:IT 499$ on ebay] - actual price: ~570$ (to be verified)
* Antennas and gizmos at radioworld, first pass:
** MFJ-941E - antenna tuner and switch [http://radioworld.ca/product_info.php?products_id=2885 155$ at radioworld]
** MAP-G5RV 1/2 - G5RV 50' dipole antenna (10-40m) [http://radioworld.ca/product_info.php?manufacturers_id=121&products_id=7788 85$ at radioworld]
** <del>J146/440 - dual band VHF/UHF antenna (2m-70cm) [http://radioworld.ca/product_info.php?cPath=73_191_193&products_id=886 40$ at radioworld]</del> /!\ backorder
** 100' of RG8 coax cabling [http://radioworld.ca/product_info.php?cPath=73_394&products_id=6831 65$ at radioworld]
** <del>MFJ-260C - 300W dummy load  0-150Mhz dry [http://radioworld.ca/product_info.php?products_id=8098 50$ at radiowrodl]</del> built into the tuner now
** 3 PL259 connectors [http://radioworld.ca/product_info.php?cPath=73_394&products_id=3244 4$ at radioworld]
** Total, incl. shipping: 452.35$
* Ferrites: ~40$ + 24$ customs fees (PN: 2643167851 from [http://www.ibselectronics.com/search_r.asp?mfgpn=2643167851 IBS electronics])
* A shitload of PL259 connectors, usually around 2$ each
* Documentation:
** [http://www.coaxpublications.ca/  Canadian Amateur Radio Basic Qualification Study Guide]: 40$
** [https://www.rac.ca/store/operating-manual.htm The RAC Operating Manual]: 42$ (note: i didn't find this one really useful)
** ARRL Handbook 2011, Softcover: 49.95$USD
** ARRL Antenna Book: 44.95$USD
* Total rig cost so far: 1242.60$ (not counting the quad project below)

I uploaded a few photos [http://photos.anarcat.koumbit.org/main.php?g2_itemId=4619 in this album].

== New quad antenna project ==

I am working on building a new antenna. It is based on [http://www.hamuniverse.com/n1uue2el1011quad.html this design], which uses a mix of PVC pipes and fiberglass rods, except I adapt it to a 20m antenna and replace the PVC with aluminium for solidity. It should be possible to take the antenna apart and rebuild it if necessary.

=== Parts list ===

[[File:Quad-sketch0001.jpg|400px|thumb|right|A sketch of my antenna]] 

{| class="wikitable"
|+
!colspan="6"|Mast
|+
! Part               || Amount || Where || Weight || Price || Notes
|-
| tripod             || 1    || addison    || ? || 45.56$ || ~2' x 2'
|-
| 3m steel pole      || 1    || downstairs || ? || 0$ || to connect the boom and tripod, 1-1¼"
|-
| sandbags           || 3    || home depot || 30kg ea. || 10.76$ || one per leg
|-
| plywood            || 3'x4' || attic || ? || 0$ || <s>1' x 2' used for the beam/pole fitting</s> - found some
|-
| Mast clamps (1"3/4)|| 4 || hardware store || ? || 4.92$ || to connect the boom with the mast, bigger mast, 1.23¢ ea
|-
| Mast clamps (1"3/8)|| 2 || hardware store || ? || 1.60$ || to connect the boom with the mast, 80¢ ea
|-
| guy rope           || 2x100' || hardware store || ? || 63.98$ || to secure the mast, using poplypropylene/nylon rope, each line should support around 300lbs of pressure, enough for the 500lbs of pressure with a 90mph wind
|+
!colspan="6"|Antenna and accessories
|+
! Part               || Amount || Where || Weight || Price || Notes
|-
| 1" hose clamps        || 8    || home depot || ? || ? || to tie the antenna line to the spreaders
|-
| tie wraps, small   || 100 || hardware store || ? || 3.09$ || to tie the antenna line to the spreaders
|-
| 2"½ hose clamps    || 19 || hardware store || ? || 26.41$ || to tie-up the boom together (2), the spreaders to the spider (8) and the spreaders themselves (8), +spare (1)
|-
| 8"x½" plastic tube || 1    || home depot || ~0 ||  0$|| for spreader/wire attachment - found some
|-
| 15' 1¼" fiberglass || 8    || [http://www.mgs4u.com/fiberglass-cubical-quad-spreaders.htm mgs4u.com] (type 1) || 17.6lbs || ~160$ - actual: 255$CAD (!!)  || 4.4lbs ea, unit price 19.80$
|-
| Clamps     || 10-pack || [http://www.dxengineering.com/Parts.asp?ID=1868&PLID=161&SecID=129&DeptID=36&PartNo=DXE-CPC-375 dxengineering] || ~0 || 14.95$ || to secure the feedline
|-
| 4m aluminium boom  || 2     ||  [http://www.dxengineering.com/Parts.asp?ID=5107&PLID=364&SecID=136&DeptID=43&PartNo=DXE-AT1492 DX engineering] || 2.5lbs || 24.90$ ||
|-
| 500' of wiring     || 1     || [http://www.dxengineering.com/Parts.asp?ID=2012&PLID=159&SecID=77&DeptID=40&PartNo=DXE-ANTW-500 DX engineering] || N/A    || 74.95$ || only parts of this will be used (120' to be more precise)
|-
| U clamps and bolts || 4    || <s>home depot</s> [http://www.rickadams.net/ Rick Adams] || ~0 || 0$ || included in the beam/pole fitting
|-
| boom/pole plate    || 1    || [http://www.rickadams.net/ Rick Adams]  || ? || 45$ + 30$ shipping || need to drill holes, includes reinforcement pole
|-
| spider             || 2     || [http://www.rickadams.net/ Rick Adams] || 2.3lbs || 70$USD || 14oz ea. 40$/ea, seems strong and sturdy, and Rick helped me through email, let's try!
|+
!colspan="6"|Antenna (parts in shipping)
|-
|colspan="6"|Nothing!
|+
!colspan="6"|Antenna (parts to order)
|+
! Part               || Amount || Where || Weight || Price || Notes
|-
| more sandbags?     || 3  || hardware store || ? || ? || to attach the tower?
|}

Total planned price: ~373.77$

Total planned weight: ~25lbs++ - not including 180lbs of sandbags, the pole and tripod.

=== Notes ===

* Gauge: 10-16 AWG is okay (see [http://en.wikipedia.org/wiki/American_wire_gauge AWG], that is 1.2-2.5mm or 1/10" - 1/20"), and can be insulated, according to [http://n6ach.com/calc/quad.html this page]
* Using welding wire is a good idea, as the [http://www.qsl.net/w3df/quad2/quad.html lightning bolt quad] uses
* I was suggested [http://www.dubo.qc.ca/ Dubo] for electric supplies

=== Current blocker(s) ===

Waiting for parts to be shipped and for the rain to stop.

=== Discarded approaches ===

Those parts were considered during the design phase but were discarded for various reasons.

{| class="wikitable"
|+
! Part               || Amount || Where || Price || Notes
|-
| welding wire aluminium || 270' spool || [http://www.canadiantire.ca/AST/browse/6/Tools/WeldingSoldering/AccessoriesRodsWire.jsp canadian tire]? || ? || example: [http://www.harrisproductsgroup.com/en/Products/Alloys/Welding/Aluminum-Alloy/Alloy-5356-MIG.aspx Aluminium 5356 MIG alloy] - too hard to find
|-
| <s>4m 1"PVC</s>           || 1    || home depot || ? || according to Rick, this could create interference
|-
| <s>5-way 1"PVC conn.</s>  || 2    || [http://www.creativeshelters.com/Fittings/PVC-Structure-Fitting.aspx creativeshelters.com] || 3.08$ || couldn't order, they don't accept canadian zip codes, and shipping is 30$ anyways, and I don't trust PVC
|-
| <s>1"x½" copper pipe</s>  || 8    || home depot || ~5$ || we'll use tie-wraps instead for spreader/wire attachment
|-
| <s>8m ½"PVC</s>           || 4    || home depot || ? || PVC pipes are too flexible to hold properly, we need fiberglass
|-
| <s>AUG 12 copper wire</s> || 44.4m  || ???   || 29.32$ (addison?) 48.64$ pour 2x12 22m (dubo) || <s>addison doesn't have bare wire, and [http://www.dubo.qc.ca/ dubo] only has bare up to 8 gage, too big (it's mainly for grounds)</s> copper wire never comes bare, and i may have better luck finding mig soldering wire like [http://www.harrisproductsgroup.com/en/Products/Alloys/Welding/Aluminum-Alloy/Alloy-5356-MIG.aspx Aluminium-5356]
|}

I had a lot of problems finding proper parts (pipes, raw materials like wiring and so on) in Montreal, which made building the antenna quite tricky, as a lot of things needed to be ordered online, which raised the overall costs because of shipping.

= Références =

Je collecte ici des bons sites au sujet du ham radio.

* Manuals, courses
** [http://www.visi.com/~tneu/whatsham.html Good overview]
** [http://www.emergencyradio.ca/course/ Emergencyradio.ca online course]
** [http://kb6nu.com/tech-manual/ tech manual]
** [http://www.baproducts.com/ham.htm HAM Radio primer] - un peu vieux (14 ans!) dit que son kit a coûté 1000$
* Hardware
** [http://batlabs.com/ hardware guide] - motorola
* Exams
** [http://www.ic.gc.ca/eic/site/smt-gst.nsf/fra/h_sf01709.html certification pour licence canadienne]
** [http://wiki.hope.net/index.php/TLH_Amateur_Radio_License_Exams Exams at the Last HOPE]
* Clubs
** [http://www.marc.qc.ca/ Montreal Amateur Radio Club] - Dorval est. 1932, mostly english and west-island-ish
** [http://www.ve2clm.ca/ Rive sud]
** [http://www.rac.ca/ Radio Amateur Canada]
* Other documentation
** [http://www.irlp.net/ IRLP] -  Internet Radio Linking Project - using the internet to link stations
** [http://hamcall.net/call Call sign lookup]
** [http://www.rac.ca/acl/ Available call signs in Canada]
** [http://www.arrl.org/FandES/field/regulations/bands.html US Ham bands]
** [[Wikipedia:Family_Radio_Service]] - intéressant pour commencer, comme le CB mais plus puissant et sans interférence, pas cher, j'ai acheté un [http://radioworld.ca/product_info.php?cPath=161_381&products_id=5790 cobra FRS]

== Software ==

This needs to be merged into [[Software]]

* the oracles
** http://packages.debian.org/gpredict
** http://packages.debian.org/minimuf - allows you to compute the WikiPedia:MUF depending on salar activity and so on, but has an unusuable interface (you need to enter a series of digits... how about a GUI?!)
** http://packages.debian.org/gcb - calculates the right angle for your antenna
** http://packages.debian.org/xplanet - can show azimuthal projections of the earth on your background, screensaver or window - I use this in myxsession:
   xplanet -latitude 45.5 -longitude -73.66 -wait 60 -label -projection azimuthal -fork -radius 90
* testing tools
** http://packages.debian.org/ibp - very useful for training to receive distant comms: show you which beacon is active when, with a map
   ibp FN35EM
* packet:
** http://packages.debian.org/gmfsk - for packet radio
** http://packages.debian.org/fldigi - also looks interesting and fairly complete, not tested
** http://packages.debian.org/gpsk31, DebianPackage:linpsk, DebianPackage:phaseshift - same?
** http://packages.debian.org/fbb - mailboxes?
* to be tested:
** http://packages.debian.org/grig - can control your radio from your computer
** http://packages.debian.org/splat 
** http://packages.debian.org/wwl
** http://packages.debian.org/wsjt
** http://packages.debian.org/xastir
** http://packages.debian.org/xwota
** http://packages.debian.org/xdemorse
* dismissed:
** http://packages.debian.org/predict and http://packages.debian.org/predict-gsat - console only, doesn't bring much the gorgeous visuals of DebianPackage:gpredict

... others to follow, there's a whole [http://packages.debian.org/sid/hamradio/ hamradio section] in debian.

= Stuff to buy next =

* VHF/UHF SWR meter
** [http://www.universal-radio.com/catalog/meters/2140.html Daiwa CN-801V] 150$ 140-525 MHz.	20/200 Watts ([http://www.eham.net/reviews/detail/7378 5 reviews: 4.2/5])
** [http://www.ebay.com/itm/SWR-Power-500-Watt-METER-120-500-MHz-UHF-VHF-Ham-Radio-w-RG8X-Jumper-/380424888249 workman 50$]  ([http://www.eham.net/reviews/detail/3905 17 reviews: 3.5/5])
** [http://www.universal-radio.com/catalog/meters/1739.html the bird 43] 85$ (but only one way at a time) 200-500 MHz.	    50 Watts ([http://www.eham.net/reviews/detail/7378 39 reviews: 4.5/5])
* Handhelds:
** Baofeng UV-3R MKII radio (<50$) avec cable de prog ou UV-5R
* Cabling:
** 100' of RG8 cabling
* Another HF radio?
** [http://shop.ebay.com/Fixed-/163857/i.html ebay]
** [http://radioworld.ca/index.php?cPath=184_72 radioworld]
** [http://www.raqi.ca/~ve2bzl/hf.htm local]
* [http://www.paratonnerres.qc.ca/produits.html Call those guys?]
