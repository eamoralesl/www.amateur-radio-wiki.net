== Structure of Atoms ==

The simplest model of atomic structure has each atom being made up of a nucleus containing [http://en.wikipedia.org/wiki/Proton Protons] and [http://en.wikipedia.org/wiki/Neutron Neutrons], with [http://en.wikipedia.org/wiki/Electron electrons], circling the nucleus at various energy levels (layers). Each layer can only hold a certain number of electrons - once a layer is full, any extra electrons must occupy a higher layer.

* electrons are very small and have a negative charge
* protons are larger and have a positive charge equal and opposite in size to that of electrons
* neutrons are the largest particle and have no charge
* the number of protons equals the number of electrons, rendering an atom charge free

The outermost layer or shell of electrons is known as the Valency Layer or Valency Shell. It is this layer that determines whether an element is a conductor or an insulator. 

'''Insulators''' have the following atomic characteristics:
* the topmost electron layer (the valency layer) is full and can hold no more electrons
* there is a large energy difference between the full layer and the one above it.

The effect of these two characteristics is that for current to flow (achieved in part by the movement of electrons from one layer to another) large energies must be applied to the "full shell" electrons to get them to move up to the next layer.  Insulators only conduct when the energy applied to them is enough to move the electrons.

'''Conductors''' (metals) have the following characteristics:

* the topmost electron layer is not empty.
* atoms may share electrons with neighbouring atoms to achieve full outer layers
* atoms are free to move from one atom to another because the energy difference to do so is zero. 

'''Semiconductors''' have the following characteristics:

* the topmost electron layer (the valency layer) is not full but when the atoms arrange themselves in a crystalline structure and share electrons the outer shell is effectively full. This renders them to be insulators
* there is a very small energy difference between the full layer and the one above it. The effect of this is that under certain conditions, semiconductors can in fact conduct, because it is relatively easy to dislodge electrons in the outer shell.

==Doping==

The electrical properties of semiconductor materials (the relative ease of dislodging electrons) can be enhanced by the addition of impurities into the crystalline structure.  For example adding Boron at 1 part in 100 000 increases the conductivity of Silicon by a factor of about 1000. Doping materials provide either extra electrons - producing N type semiconductors, or a "holes" into which electrons can move - producing P type semiconductors.

==Further reading==

* [http://en.wikipedia.org/wiki/Semiconductor Wikipedia]
* [http://en.wikipedia.org/wiki/List_of_semiconductor_materials Wikipedia list of semiconductor materials]
* [http://www.howstuffworks.com/diode.htm How Stuff Works]
