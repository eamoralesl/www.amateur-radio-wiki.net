Related wiki pages :  [[Education]], [[Electronic Theory]], [[Videos#Tutorials|Tutorials]], [[User Groups]], [[Ham Radio for Kids]]

Learning ham radio is an ever lasting process. Whether you are an experienced operator or a newcomer, there's always a lot to learn. But if you are new, it can be difficult to find how to learn and where to start. This page tries to simplify this for you.

= Learning the basics =

''Main article: [[What is amateur radio?]]''

The first thing you should do is get educated. A lot of operators have learned by themselves, but most people also look for help with local [[Clubs]] and find a mentor to help them if they have questions. [[Courses]] are available for people that are not easily self-taught and [[books]] can help everybody.

On top of radio-specific knowledge, ham tests usually feature questions about electronics, so you will need to be familiar with [[Electronic Theory]] at the very least. Most countries have abandoned [[morse code]] requirements although it is very useful to make distant contacts.

To study for the ham radio test itself there are several good [[Online Study Resources]].

= Getting a license =

Getting a license is usually done by passing an exam with an accredited operator. This varies a lot according to the country you are in, so you will probably want to consult the local [[club]] documentation to see what the question sets are and where to pass the exam. You can usually find [[websites]] where you can practice the exam in advance, which is usually multi-choice. In some places, you need to pass a specific test (in the US, this is the [[Advanced qualification]]) or have a special score (e.g. in Canada you need 80%) to have access to [[HF]] frequencies that allow you to communicate really long distances (through [[sky waves]]).

When you pass your exam, you will have to choose a [[callsign]] which will identify you uniquely over all the operators in the world.

= Finding hardware =

''Main article: [[Apparatus]]''

Once you have a license, you can start looking at getting [[hardware]]. While you can get (and in some places operate, to a limited extent) hardware without having a license, it is recommended that you first get a license, for two reasons: 

# you will know better what you are looking for and why
# you will have better contacts in the community to help you find proper material

You can find radio hardware in a lot of places around the world. Most people deal with [[suppliers]] directly, as it will allow you to have a warranty on your material and support if it breaks down. Another good place to get hardware is to look for your local [[hamfest]] as you will be able to talk with real operators that can help you choose according to your needs. 

You can also find [[classified ads]] online where people exchange their geir. Finally, you can also look for online auction sites. Ebay, for example, has a fairly large [http://electronics.shop.ebay.com/Ham-Radio-/4670/i.html Ham radio section]. Note that buying online has its risks and it can happen that your hardware arrives damaged or simply non-functional.

What hardware should you get now is the big question, for this we need to look at how to build a [[station]].

= Building a station =

A [[station]] is really just a radio with an antenna, operated (or "worked") by a ham. It can be as simple as a handheld receiver ([[antenna]], [[transceiver]] and [[batteries]] included!) or as complex a multi-radio rig with remote-controlled motorized antennas.

Usually, of course, you will start with something simple. A hand-held or mobile radio is a good idea, as they mostly work out of the box (although you may need to get an antenna for some mobile radios). Do not go right for the most expensive gadget with all the flashing leds, as you are just starting.

If you get a fixed or mobile station, you may need a power supply. Older stations also need an [[SWR bridge]] and if your antenna is not designed for your radio, you will need an [[antenna tuner]] (see also the article on [[impedance]]).

There are zillion more things you can add to the pool, but that's basically it: an [[antenna]], a [[receiver]] (or better, a [[transceiver]]), [[power]] and a good [[ground]] is all you really need. See the [[apparatus]] page for more details.

= Operating the station =

''Main article: [[Operating_procedures]]''

Once you have hardware, you will need to operate it. For this you will need to get familiar with the various [[modes]] and [[bands]] used in amateur radio. You also need to understand how radio [[propagation]] works to use the right band and mode. You will certainly need to get familiar with the [[ham jargon]] a little before getting on the air. You also need to be familiar with [[emergency communications]] and [[regulations]].

You can start by just listening. If you are careful not to transmit anything, you can't screw up and nobody will notice. Try to locate a [[beacon]] you now is near you. Then try to locate one far away. Try to pickup conversations from other operators. Don't forget to look at the [[band plan]] to see where people are likely to be and what [[mode]] they will be using, otherwise you will just find [[noise]].

Once you feel comfortable, you can try to establish your first contact, with a traditional [[CQ]]. This is where a mentor can be of use: if you can communicate out of band with the person, it can help you diagnose things you may have done wrong.

= Going further ahead =

From there on, sky (or rather, space!) is the limit really. You can start digging more around this wiki or, even better, [[Project:Contributing|contribute]] what you have learned in the process here.

= Country-specific advice =

* [[Getting licensed in Amateur Radio is easier than you think]] - USA
* [[Learning Amateur Radio in Canada]]
