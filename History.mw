Related wiki pages : [[Women in amateur radio]]

== Communication before Radio ==

The history of radio communication is really a part of the history of communication, with particular reference to the electromagnetic spectrum. It is argued by some historians that the first recoded transmission in the electromagnetic spectrum is in Homer's Iliad (about 1200BC) where communication through a series of fires is described.

There are also reports of mirrors being used by the Persian army in about 500BC.

== Essential Discoveries ==

Most of the fundamental electrical knowledge that lead to the development of radio was discoveres in the 1500's to the 1800's. Scientists of note include [http://en.wikipedia.org/wiki/Alessandro_Volta Alessandro Volta], [http://en.wikipedia.org/wiki/Georg_Ohm Georg Ohm], [http://en.wikipedia.org/wiki/Andr%C3%A9-Marie_Amp%C3%A8re Andre-Marie Ampere] and [http://en.wikipedia.org/wiki/Charles_Wheatstone Charles Wheatstone].

In 1610, [http://en.wikipedia.org/wiki/Sunspots sunspots] were discovered by [http://en.wikipedia.org/wiki/Galileo Galileo] but their influence on communication was not recognised for many years.

The telegraph was invented in 1823 by Sir Francis Reynolds, and after initially being ignored, was developed throughout the middle 1800's to become a successful communication medium. The communication "language" used was [http://en.wikipedia.org/wiki/Morse_code Morse Code], invented by [http://en.wikipedia.org/wiki/Samuel_F._B._Morse Samuel Morse]. It is still used today by radio amateurs in [[Modes#Continuous Wave (CW)|CW]].

During the 1800's a number of scientists contributed greatly to the understanding of [http://en.wikipedia.org/wiki/Electromagnetism Electromagnetism], perhaps the most influential of them being [http://en.wikipedia.org/wiki/James_Clerk_Maxwell James Clerk Maxwell] who in 1861 predicted the existance of electomagnetica waves. There existance was not confirmed or demonstrated until [http://en.wikipedia.org/wiki/Heinrich_Hertz Heinrich Hertz] did so in 1888.  A flurry of activity in the late 1880's including public demonstrations by in 1891 [http://fr.wikipedia.org/wiki/Nikola_Tesla Nikola Tesla] and successful communication over 2 miles in 1896 by Marconi.

Ham radio can be thought to have started in 1898 when Leslie Miller encouraged experimenters to work on the then new field of wireless.

== Notable dates ==

* 1886 - Hertz first experiments with radio waves and induction
* 1891 - Tesla demonstrates transmission of energy through radio frequencies
* 1896 - Bose performs remote operations through radio signals
* 1899 - Marconi sends a signal across the English Channel
* 1900 - Duddle described and made the first LC tuned circuit
* 1901 - Marconi sends a signal across the Atlantic
* 1902 - Heaviside predicts the existence of "conducting layers" in the atmosphere.
* 1905 - Marconi invents and patents first horizontal antenna
* 1906 - First wireless communication of speech
* 1906 - [[Triode]] vacuum tube invented
* 1909 - First amateur radio club is formed in New York City
* 1910 - [[Detector#Cat Whisker|"Cat Whisker" detector]] invented
* 1910 - First amateur "[[callbook]]" issued
* 1912 - [[Radio Act of 1912]] restricts private stations to wavelengths of 200 meters or less
* 1915 - Kellogg invents the moving-coil loudspeaker
* 1918 - Nicholson invents the crystal-controlled [[oscillator]]
* 1918 - Armstrong invents the [[superheterodyne]] receiver
* 1923 - [[Modes#Single-Sideband Modulation (SSB)|Single Sideband (SSB)]] invented
* 1926 - Yagi and Uda invent the beam antenna now known as the [[Yagi]]
* 1927 - First International Radiotelegraph Conference establishes [[80 metres|80]], [[40 metres|40]], [[20 metres|20]] and [[10 metres|10]] metre bands, and amateur callsigns
* 1933 - Crystal microphone invented
* 1947 - [[Heathkit]] introduces its first electronic kit
* 1960 - First [[Earth-Moon-Earth]] communication
* 1961 - First OSCAR (Orbiting Satellite Carrying Amateur Radio) satellite sent into orbit
* 1979 - WARC bands created (12, 17, and 30 metres)

== Local Histories ==

* [[Ham Radio History - Australia]]
* [[Ham Radio History - Canada]]
* [[Ham Radio History - New Zealand]]
* [[Ham Radio History - United Kingdom]]
* [[Ham Radio History - USA]]

== External links ==

=== Wikipedia ===

* [http://en.wikipedia.org/wiki/History_of_amateur_radio History of amateur radio]
* [http://en.wikipedia.org/wiki/History_of_radio History of radio]
* [http://en.wikipedia.org/wiki/Invention_of_radio Invention of radio]

=== Others ===

* [http://www.ac6v.com/history.htm Detailed history]
* [http://www.astrosurf.com/luxorion/qsl-ham-history-landmarks.htm Cleaner detailed history, somehow Belgium-centric]

{{history}}
